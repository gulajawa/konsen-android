package net.arizalsaputro.konsen.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.databinding.ItemRankingBinding;
import net.arizalsaputro.konsen.model.Person;
import net.arizalsaputro.konsen.viewmodel.ItemPersonVewModel;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by muharizals on 06/04/2017.
 */

public class ListPersonAdapter extends RecyclerView.Adapter<ListPersonAdapter.ListPersonAdapterViewHolder>{
    private ArrayList<Person> list;

    public ListPersonAdapter(){
        list = new ArrayList<>();
    }

    public ListPersonAdapter(ArrayList<Person> list) {
        this.list = list;
    }

    @Override
    public ListPersonAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemRankingBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_ranking,parent,false);

        return new ListPersonAdapterViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ListPersonAdapterViewHolder holder, int position) {
        holder.bindPerson(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setList(ArrayList<Person> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public static class ListPersonAdapterViewHolder extends RecyclerView.ViewHolder{
        ItemRankingBinding itemRankingBinding;
        public ListPersonAdapterViewHolder(ItemRankingBinding itemRankingBinding) {
            super(itemRankingBinding.getRoot());
            this.itemRankingBinding= itemRankingBinding;
        }

        public void bindPerson(Person person){
            if(itemRankingBinding.getPerson() == null){
                itemRankingBinding.setPerson(new ItemPersonVewModel(person));
            }else{
                itemRankingBinding.getPerson().setPerson(person);
            }
        }
    }
}
