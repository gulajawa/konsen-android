package net.arizalsaputro.konsen.view.game;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.databinding.ActivityOperationGameBinding;
import net.arizalsaputro.konsen.helper.MyMediaHelper;
import net.arizalsaputro.konsen.util.Constant;
import net.arizalsaputro.konsen.util.MySharedPreference;
import net.arizalsaputro.konsen.view.AppBaseInterface;
import net.arizalsaputro.konsen.view.MainActivity;
import net.arizalsaputro.konsen.viewmodel.game.OperationGameViewModel;

import az.plainpie.animation.PieAngleAnimation;


public class OperationGameActivity extends AppCompatActivity implements AppBaseInterface{

    private ActivityOperationGameBinding binding;
    private OperationGameViewModel viewModel;

    private MySharedPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_operation_game);
        initialBinding();

        preference = new MySharedPreference(this);

        setupViewModel();
        showDialog();

    }

    private void initialBinding(){
        binding = DataBindingUtil.setContentView(this,R.layout.activity_operation_game);
    }

    private void supportActionBar(Toolbar toolbar){
        toolbar.setTitle(getString(R.string.game_opration));
        setSupportActionBar(toolbar);
    }

    private void setupViewModel(){
        viewModel = new OperationGameViewModel(this,this);
        binding.setGameOperation(viewModel);
    }

    private void showDialog(){
        if(preference.isShowDialog(Constant.SHOW_DIALOG_OPERATION)){
            new MaterialStyledDialog.Builder(this)
                    .setTitle(getString(R.string.how_to_play))
                    .setDescription(getString(R.string.operation_play))
                    .setIcon(R.drawable.ic_operation)
                    .setHeaderColor(R.color.cloud)
                    .setCancelable(false)
                    .withDarkerOverlay(true)
                    .withDialogAnimation(true)
                    .setPositiveText(getString(R.string.understand))
                    .setNeutralText(getString(R.string.dont_show_again))
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    })
                    .onNeutral(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                            preference.dontShowDialog(Constant.SHOW_DIALOG_OPERATION);
                        }
                    })
                    .show();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        if(preference.isMusicOn()){
            MainActivity.that.pauseMusic();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(preference.isMusicOn()){
            MainActivity.that.startMusic();
        }
    }



    @Override
    public void onBackPressed() {
        viewModel.cancel();
        super.onBackPressed();
    }


    @Override
    public void setToolbar() {
        binding.toolbar.setTitle(getString(R.string.result));
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void update() {
        PieAngleAnimation animation = new PieAngleAnimation(binding.pieView);
        animation.setDuration(1000);
        binding.pieView.startAnimation(animation);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
