package net.arizalsaputro.konsen.view;

/**
 * Created by muharizals on 04/04/2017.
 */

public interface AppBaseInterface {
    void setToolbar();
    void update();
}
