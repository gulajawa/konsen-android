package net.arizalsaputro.konsen.view.fragments;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.databinding.FragmentRankingBinding;
import net.arizalsaputro.konsen.view.AppBaseInterface;
import net.arizalsaputro.konsen.view.adapter.ListPersonAdapter;
import net.arizalsaputro.konsen.viewmodel.RankingViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class RankingFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener,AppBaseInterface{

    private FragmentRankingBinding binding;
    private RankingViewModel viewModel;

    public RankingFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_ranking,container,false);
        setupViewModel();
        setupRefreshLayout(binding.swipeContainer);
        setupListPersonView(binding.recyclerView);
        viewModel.getRankingListData();
        return binding.getRoot();
    }

    private void setupViewModel(){
        viewModel = new RankingViewModel(getContext(),this);
        binding.setRanking(viewModel);
    }

    private void setupRefreshLayout(SwipeRefreshLayout swipeRefreshLayout){
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh() {
        viewModel.getRankingListData();
    }

    private void setupListPersonView(RecyclerView recyclerView){
        ListPersonAdapter adapter = new ListPersonAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    public void scrollList(){
        binding.recyclerView.smoothScrollToPosition(0);
    }

    @Override
    public void setToolbar() {

    }

    @Override
    public void update() {
        binding.swipeContainer.setRefreshing(false);
        ListPersonAdapter adapter =(ListPersonAdapter) binding.recyclerView.getAdapter();
        adapter.setList(viewModel.getList());
        if(viewModel.getList().size() <= 0){
            binding.recyclerView.setVisibility(View.GONE);
            binding.textNoData.setVisibility(View.VISIBLE);
        }else{
            binding.recyclerView.setVisibility(View.VISIBLE);
            binding.textNoData.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        viewModel.reset();
    }
}
