package net.arizalsaputro.konsen.view;

/**
 * Created by muharizals on 09/04/2017.
 */

public interface ChallangeInterface {
    void update();
    void surrender();
    void start();
}
