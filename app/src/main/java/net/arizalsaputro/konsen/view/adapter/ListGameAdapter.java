package net.arizalsaputro.konsen.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.databinding.ItemGameBinding;
import net.arizalsaputro.konsen.model.Game;
import net.arizalsaputro.konsen.viewmodel.ItemGameViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by muharizals on 03/04/2017.
 */

public class ListGameAdapter extends RecyclerView.Adapter<ListGameAdapter.ListGameAdapterViewHolder> {
    private List<Game> gameList;

    public ListGameAdapter(){
        gameList = new ArrayList<>();
    }

    public void setGameList(List<Game> gameList) {
        this.gameList = gameList;
        notifyDataSetChanged();
    }

    public ListGameAdapter(List<Game> gameList) {
        this.gameList = gameList;
    }

    @Override
    public ListGameAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemGameBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_game,parent,false);
        return new ListGameAdapterViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ListGameAdapterViewHolder holder, int position) {
        holder.bindGame(gameList.get(position));
    }

    @Override
    public int getItemCount() {
        return gameList.size();
    }



    public static class ListGameAdapterViewHolder extends RecyclerView.ViewHolder{
        ItemGameBinding itemGameBinding;

        public ListGameAdapterViewHolder(ItemGameBinding itemGameBinding) {
            super(itemGameBinding.itemGame);
            this.itemGameBinding = itemGameBinding;
        }

        public void bindGame(Game game){
            if (itemGameBinding.getItemGame() == null){
                itemGameBinding.setItemGame(new ItemGameViewModel(game,itemView.getContext()));
            }else{
                itemGameBinding.getItemGame().setGame(game);
            }
        }
    }
}
