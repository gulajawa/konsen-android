package net.arizalsaputro.konsen.view.challange;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.databinding.FragmentOperationChallangeBinding;
import net.arizalsaputro.konsen.util.Constant;
import net.arizalsaputro.konsen.viewmodel.challange.OperationChallangeViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class OperationChallange extends Fragment {

    private FragmentOperationChallangeBinding binding;
    private OperationChallangeViewModel viewModel;

    public OperationChallange() {
        // Required empty public constructor

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_operation_challange,container,false);
        viewModel = new OperationChallangeViewModel(getContext());
        binding.setGameOperation(viewModel);
        viewModel.start();
        return binding.getRoot();
    }


}
