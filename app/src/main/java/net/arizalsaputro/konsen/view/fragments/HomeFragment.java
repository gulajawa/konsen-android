package net.arizalsaputro.konsen.view.fragments;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dzaitsev.android.widget.RadarChartView;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.databinding.FragmentHomeBinding;
import net.arizalsaputro.konsen.helper.SkillHelper;
import net.arizalsaputro.konsen.viewmodel.HomeViewModel;

import java.util.LinkedHashMap;
import java.util.Map;

import static android.graphics.Paint.Style.FILL;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private FragmentHomeBinding binding;
    private SkillHelper skillHelper;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_home,container,false);
        skillHelper = new SkillHelper(getContext());
        initData(binding.radarChart);
        initViewModel();
        return binding.getRoot();
    }

    private void initViewModel(){
        HomeViewModel homeViewModel = new HomeViewModel(getContext());
        binding.setHomeViewModel(homeViewModel);
    }

    private void initData(RadarChartView chartView){
        Map<String,Float> axis = new LinkedHashMap<>(6);

        axis.put(getString(R.string.c_speed),skillHelper.getSpeed());
     /*   axis.put(getString(R.string.c_memory),10F);*/
        axis.put(getString(R.string.c_accuracy),skillHelper.getAccuracy());
        axis.put(getString(R.string.c_calculation),skillHelper.getCalculation());
       /* axis.put(getString(R.string.c_judgment),skillHelper.getJudgment());
        axis.put(getString(R.string.c_obeservation),10F);*/

        chartView.setAxis(axis);
        chartView.setAxisMax(100F);
        chartView.setCirclesOnly(true);
        chartView.setAutoSize(true);             // auto balance the chart// if you want circles instead of polygons
        chartView.setChartStyle(FILL);
    }
}
