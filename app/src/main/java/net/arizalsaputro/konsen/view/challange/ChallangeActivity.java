package net.arizalsaputro.konsen.view.challange;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.databinding.ActivityChallangeBinding;
import net.arizalsaputro.konsen.util.Constant;
import net.arizalsaputro.konsen.util.MySharedPreference;
import net.arizalsaputro.konsen.view.AppBaseInterface;
import net.arizalsaputro.konsen.view.ChallangeInterface;
import net.arizalsaputro.konsen.view.MainActivity;
import net.arizalsaputro.konsen.viewmodel.challange.ChallangeViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ChallangeActivity extends AppCompatActivity implements ChallangeInterface{

    public static Intent start(Context context){
        return new Intent(context,ChallangeActivity.class);
    }

    private ActivityChallangeBinding binding;
    private ChallangeViewModel viewModel;
    private MySharedPreference preference;

    private List<Fragment> listgame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_challange);

        preference = new MySharedPreference(this);
        initGame();
        initialBinding();
        setupViewModel();

    }

    private void initGame(){
        listgame = new ArrayList<>();
        listgame.add(new OperationChallange());
        listgame.add(new BirdEyeChallangeFragment());
        listgame.add(new ColorDeceptionChallange());
    }

    private void initialBinding(){
        binding = DataBindingUtil.setContentView(this,R.layout.activity_challange);
    }

    private void setupViewModel(){
        viewModel = new ChallangeViewModel(this,this,listgame.size());
        binding.setChallange(viewModel);
    }

    public void backToHome(View view){
        try{
            finish();
        }catch (Exception e){

        }

    }

    @Override
    public void update() {

    }

    @Override
    public void surrender() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new MaterialStyledDialog.Builder(ChallangeActivity.this)
                        .setDescription(getString(R.string.your_opponent_give_up))
                        .setIcon(R.mipmap.ic_launcher)
                        .setHeaderColor(R.color.cloud)
                        .setCancelable(false)
                        .withDarkerOverlay(true)
                        .withDialogAnimation(true)
                        .setPositiveText(getString(R.string.understand))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                finish();
                            }
                        })
                        .show();
            }
        });

    }


    @Override
    public void start() {
        Handler handler= new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                Fragment challange = null;
                try{
                    challange = listgame.get(viewModel.getGamePosition());
                }catch (Exception e){
                    challange = listgame.get(0);
                }
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(binding.frame.getId(),challange).commit();
            }
        });
    }


    @Override
    public void onBackPressed() {

        if(viewModel.isdone){
            super.onBackPressed();
            return;
        }

        if(viewModel.haveOpponent){
            new MaterialStyledDialog.Builder(this)
                    .setTitle(getString(R.string.surrender))
                    .setDescription(getString(R.string.sure_surrender))
                    .setIcon(R.mipmap.ic_launcher)
                    .setHeaderColor(R.color.river)
                    .setCancelable(true)
                    .withDarkerOverlay(true)
                    .withDialogAnimation(true)
                    .setPositiveText(getString(R.string.yes))
                    .setNegativeText(getString(R.string.no))
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            ChallangeActivity.super.onBackPressed();
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        }else{
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(preference.isMusicOn()){
            MainActivity.that.pauseMusic();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(preference.isMusicOn()){
            try{
                MainActivity.that.startMusic();
            }catch (Exception e){

            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.reset();
    }
}
