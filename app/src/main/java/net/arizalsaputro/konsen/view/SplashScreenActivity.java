package net.arizalsaputro.konsen.view;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.jrummyapps.android.widget.AnimatedSvgView;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.helper.BeepHelper;

public class SplashScreenActivity extends AppCompatActivity {

    BeepHelper beepHelper ;
    AnimatedSvgView svgView,svgView2;
    CountDownTimer timer1,timer2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_splash);

        svgView = (AnimatedSvgView) findViewById(R.id.animated_svg_view);
        svgView2 = (AnimatedSvgView) findViewById(R.id.animated_svg_view_2);

        beepHelper = new BeepHelper(this);

        timer1 = new CountDownTimer(2000,1000){

            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                svgView.setVisibility(View.GONE);
                svgView2.setVisibility(View.VISIBLE);
                svgView2.start();
                beepHelper.show();
                timer2.start();
            }
        };

        timer2 = new CountDownTimer(2000,1000){

            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                startActivity(MainActivity.start(SplashScreenActivity.this));
                finish();
            }
        };
    }


    @Override
    protected void onPause() {
        super.onPause();
        try{
            timer1.cancel();
            timer2.cancel();
        }catch (Exception e){

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        svgView.start();
        beepHelper.show();
        timer1.start();
    }
}
