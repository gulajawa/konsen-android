package net.arizalsaputro.konsen.view;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Window;
import android.view.WindowManager;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.databinding.ActivityListPlayBinding;
import net.arizalsaputro.konsen.helper.MyMediaHelper;
import net.arizalsaputro.konsen.model.Game;
import net.arizalsaputro.konsen.util.Constant;
import net.arizalsaputro.konsen.util.MySharedPreference;
import net.arizalsaputro.konsen.view.adapter.ListGameAdapter;
import net.arizalsaputro.konsen.viewmodel.ListPlayViewModel;

import java.util.List;

public class ListPlayActivity extends AppCompatActivity {

    public static Intent start(Context context){
        return new Intent(context,ListPlayActivity.class);
    }

    private MySharedPreference preference;
    private ActivityListPlayBinding binding;
    private ListPlayViewModel viewModel;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_list_play);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initialBinding();
        setupViewModel();
        setupListGameView(binding.myRecyclerView,viewModel.getLisGame());

        preference = new MySharedPreference(this);
    }

    private void initialBinding(){
        binding = DataBindingUtil.setContentView(this,R.layout.activity_list_play);
    }

    private void setupViewModel(){
        viewModel = new ListPlayViewModel(this);
        binding.setListPLay(viewModel);
    }

    private void setupListGameView(RecyclerView recyclerView, List<Game> list){
        ListGameAdapter adapter = new ListGameAdapter(list);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new GridLayoutManager(this, Constant.GRID_LIST_LENGTH));
    }


    @Override
    protected void onPause() {
        super.onPause();
        if(preference.isMusicOn()){
            MainActivity.that.pauseMusic();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(preference.isMusicOn()){
           try{
               MainActivity.that.startMusic();
           }catch (Exception e){

           }
        }
    }
}
