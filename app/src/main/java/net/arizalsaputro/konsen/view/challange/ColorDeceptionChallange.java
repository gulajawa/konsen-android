package net.arizalsaputro.konsen.view.challange;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.databinding.FragmentBirdEyeChallangeBinding;
import net.arizalsaputro.konsen.databinding.FragmentColorDeceptionChallangeBinding;
import net.arizalsaputro.konsen.viewmodel.challange.BirdEyeChallangeViewModel;
import net.arizalsaputro.konsen.viewmodel.challange.ColorDeceptionChallangeViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class ColorDeceptionChallange extends Fragment {

    private FragmentColorDeceptionChallangeBinding binding;
    private ColorDeceptionChallangeViewModel viewModel;


    public ColorDeceptionChallange() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_color_deception_challange,container,false);
        viewModel = new ColorDeceptionChallangeViewModel(getContext());
        binding.setGame(viewModel);
        viewModel.start();
        return binding.getRoot();
    }

}
