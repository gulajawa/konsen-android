package net.arizalsaputro.konsen.view.game;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.databinding.ActivityColorDeceptionGameBinding;
import net.arizalsaputro.konsen.helper.MyMediaHelper;
import net.arizalsaputro.konsen.util.Constant;
import net.arizalsaputro.konsen.util.MySharedPreference;
import net.arizalsaputro.konsen.view.AppBaseInterface;
import net.arizalsaputro.konsen.view.MainActivity;
import net.arizalsaputro.konsen.viewmodel.game.ColorDeceptionViewModel;
import net.arizalsaputro.konsen.viewmodel.game.OperationGameViewModel;

public class ColorDeceptionGameActivity extends AppCompatActivity implements AppBaseInterface{

    private ActivityColorDeceptionGameBinding binding;
    private ColorDeceptionViewModel viewModel;

    private MySharedPreference preference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_color_deception_game);

        preference = new MySharedPreference(this);

        initialBinding();
        setupViewModel();
        showDialog();


    }

    private void initialBinding(){
        binding = DataBindingUtil.setContentView(this,R.layout.activity_color_deception_game);
    }

    private void setupViewModel(){
        viewModel = new ColorDeceptionViewModel(this,this);
        binding.setGame(viewModel);
    }

    private void showDialog(){
        if(preference.isShowDialog(Constant.SHOW_DIALOG_COLOR_DECEPTION)){
            new MaterialStyledDialog.Builder(this)
                    .setTitle(getString(R.string.how_to_play))
                    .setDescription(getString(R.string.color_deception_play))
                    .setIcon(R.drawable.ic_color_palette)
                    .setHeaderColor(R.color.cloud)
                    .setCancelable(false)
                    .withDarkerOverlay(true)
                    .withDialogAnimation(true)
                    .setPositiveText(getString(R.string.understand))
                    .setNeutralText(getString(R.string.dont_show_again))
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                        }
                    })
                    .onNeutral(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            dialog.dismiss();
                            preference.dontShowDialog(Constant.SHOW_DIALOG_COLOR_DECEPTION);
                        }
                    })
                    .show();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        if(preference.isMusicOn()){
            MainActivity.that.pauseMusic();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(preference.isMusicOn()){
            MainActivity.that.startMusic();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setToolbar() {
        binding.toolbar.setTitle(getString(R.string.result));
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onBackPressed() {
        viewModel.cancel();
        super.onBackPressed();
    }

    @Override
    public void update() {

    }
}
