package net.arizalsaputro.konsen.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.beltaief.reactivefb.actions.ReactiveLogin;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.databinding.ActivityMainBinding;
import net.arizalsaputro.konsen.helper.BeepHelper;
import net.arizalsaputro.konsen.util.MySharedPreference;
import net.arizalsaputro.konsen.view.adapter.ViewPagerAdapter;
import net.arizalsaputro.konsen.view.fragments.AccountFragment;
import net.arizalsaputro.konsen.view.fragments.HomeFragment;
import net.arizalsaputro.konsen.view.fragments.RankingFragment;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    public static MainActivity that;

    public static Intent start(Context context){
        return new Intent(context,MainActivity.class);
    }

    public ActivityMainBinding binding;

    private MySharedPreference preference;

    private MediaPlayer mediaPlayer = null;

    private HomeFragment homeFragment;
    private RankingFragment rankingFragment;
    private BeepHelper beepHelper;
    private AccountFragment accountFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        that = this;
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        setupBinding();
        setupActionBar(binding.toolbar);
        setupFragments();
        setupViewPager(binding.viewpager);
        setupTab(binding.tabs,binding.viewpager);
        setupIcon(binding.tabs);

        preference = new MySharedPreference(this);
        beepHelper = new BeepHelper(this);

        if(preference.isMusicOn()){
            new MyMusicAycnTask().execute();
        }

    }



    private void getHashKey(){
        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo("net.arizalsaputro.konsen", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }


    private void setupBinding(){
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main);
    }

    private void setupActionBar(Toolbar toolbar){
        toolbar.setTitle(getString(R.string.app_name));
        setSupportActionBar(toolbar);
    }
    private void setupFragments(){
        homeFragment = new HomeFragment();
        rankingFragment = new RankingFragment();
        accountFragment = new AccountFragment();
    }
    private void setupViewPager(ViewPager viewPager){
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(homeFragment);
        adapter.addFragment(rankingFragment);
        adapter.addFragment(accountFragment);
        viewPager.setAdapter(adapter);

    }

    public static void toTop(CoordinatorLayout coordinator,AppBarLayout appbar){
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appbar.getLayoutParams();
        AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) params.getBehavior();
        behavior.onNestedFling(coordinator, appbar, null, 0, -1000, true);
    }

    private void setupTab(TabLayout tabLayout,ViewPager viewPager){
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                toTop(binding.coordinator,binding.appbar);
                changeTitle(tab.getPosition());
                int tabIconColor = ContextCompat.getColor(MainActivity.this, android.R.color.white);
                tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                beepHelper.click();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int tabIconColor = ContextCompat.getColor(MainActivity.this, R.color.konsen_2);
                tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                toTop(binding.coordinator,binding.appbar);
                switch (tab.getPosition()){
                    case 1: rankingFragment.scrollList();break;
                }
            }
        });


    }

    private void setupIcon(TabLayout tabLayout){
        int tabIconColor = ContextCompat.getColor(MainActivity.this, android.R.color.white);
        int tabIconColorUnselected = ContextCompat.getColor(MainActivity.this, R.color.konsen_2);
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_home);
        tabLayout.getTabAt(0).getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);

        tabLayout.getTabAt(1).setIcon(R.drawable.ic_trophy);
        tabLayout.getTabAt(1).getIcon().setColorFilter(tabIconColorUnselected, PorterDuff.Mode.SRC_IN);

        tabLayout.getTabAt(2).setIcon(R.drawable.ic_account);
        tabLayout.getTabAt(2).getIcon().setColorFilter(tabIconColorUnselected, PorterDuff.Mode.SRC_IN);



    }

    private void changeTitle(int position){
        switch (position){
            case 0: getSupportActionBar().setTitle(R.string.app_name);break;
            case 1:getSupportActionBar().setTitle(R.string.tab_ranking);break;
            case 2: getSupportActionBar().setTitle(R.string.tab_account);break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main_menu,menu);
        if(preference.isMusicOn()){
            menu.getItem(0).setIcon(R.drawable.ic_volume_up);
        }else{
            menu.getItem(0).setIcon(R.drawable.ic_volume_off);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.menu_music:
                preference.setMusicOnOff();
                if(preference.isMusicOn()){
                    item.setIcon(R.drawable.ic_volume_up);
                    new MyMusicAycnTask().execute();
                }else{
                    item.setIcon(R.drawable.ic_volume_off);
                    stopMusic();
                }
                break;
            case R.id.menu_rate_app:
                Toast.makeText(this,getString(R.string.no_supported_yet), Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_about:
                Toast.makeText(this,getString(R.string.no_supported_yet), Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public void stopMusic(){
        if(mediaPlayer != null){
            try{
                mediaPlayer.pause();
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer=null;
            }catch (Exception e){

            }
        }
    }

    public void pauseMusic(){
        if(mediaPlayer != null){
            try{
                if(mediaPlayer.isPlaying()){
                    mediaPlayer.pause();
                }
            }catch (Exception e){

            }
        }
    }

    public void startMusic(){
        if(mediaPlayer != null){
            try{
                if(!mediaPlayer.isPlaying()){
                    mediaPlayer.start();
                }
            }catch (Exception e){

            }
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        if(preference.isMusicOn()){
            pauseMusic();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(preference.isMusicOn()){
            startMusic();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(preference.isMusicOn()){
          stopMusic();
        }
    }


    @Override
    public void onBackPressed() {
        showDialogExit();
    }



    private void showDialogExit(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(getString(R.string.exit));
        builder.setMessage(getString(R.string.exit_msg));
        builder.setPositiveButton(getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    class MyMusicAycnTask extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.background_music);
            mediaPlayer.setLooping(true);
            mediaPlayer.start();
            return null;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ReactiveLogin.onActivityResult(requestCode,resultCode,data);
    }

}
