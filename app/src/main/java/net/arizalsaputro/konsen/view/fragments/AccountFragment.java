package net.arizalsaputro.konsen.view.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.beltaief.reactivefb.actions.ReactiveLogin;
import com.beltaief.reactivefb.requests.ReactiveRequest;
import com.beltaief.reactivefb.util.Utils;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.databinding.FragmentAccountBinding;
import net.arizalsaputro.konsen.view.AppBaseInterface;
import net.arizalsaputro.konsen.viewmodel.AccountViewModel;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener,AppBaseInterface{

    private FragmentAccountBinding binding;
    private AccountViewModel viewModel;

    public AccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_account,container,false);
        initViewModel();
        setupRefreshLayout(binding.swipeContainer);
        return  binding.getRoot();
    }

    private void initViewModel(){
        viewModel = new AccountViewModel(getContext(),this);
        binding.setAccount(viewModel);
    }

    private void setupRefreshLayout(SwipeRefreshLayout swipeRefreshLayout){
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        viewModel.reset();
    }


    @Override
    public void onRefresh() {
        viewModel.updateUserProfile();

    }

    @Override
    public void setToolbar() {

    }

    @Override
    public void update() {
        binding.swipeContainer.setRefreshing(false);
    }
}
