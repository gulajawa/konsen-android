package net.arizalsaputro.konsen.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.helper.BeepHelper;
import net.arizalsaputro.konsen.helper.MyMediaHelper;
import net.arizalsaputro.konsen.model.Game;
import net.arizalsaputro.konsen.util.MySharedPreference;
import net.arizalsaputro.konsen.view.ListPlayActivity;

/**
 * Created by muharizals on 03/04/2017.
 */

public class ItemGameViewModel extends BaseObservable{
    private Game game;
    private Context context;

    public ItemGameViewModel(Game game, Context context) {
        this.game = game;
        this.context = context;
    }

    public Drawable getImage() {
        return game.image;
    }

    public String getName(){
        return game.name;
    }



    @BindingAdapter("imageUrl") public static void setImageUrl(ImageView imageView, int url) {
        Glide.with(imageView.getContext()).load(url).into(imageView);
    }


    public void setGame(Game game) {
        this.game = game;
        notifyChange();
    }

    public void onItemClick(View view){
        BeepHelper.with(view.getContext()).click();
        if(game.aClass == null){
            Toast.makeText(context, context.getString(R.string.coming_soon_msg), Toast.LENGTH_SHORT).show();
        }else{

            context.startActivity(new Intent(context,game.aClass));
        }
    }

}
