package net.arizalsaputro.konsen.viewmodel;

import android.app.Activity;
import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.databinding.ObservableInt;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.beltaief.reactivefb.ReactiveFB;
import com.beltaief.reactivefb.actions.ReactiveLogin;
import com.beltaief.reactivefb.requests.ReactiveRequest;
import com.bumptech.glide.Glide;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;

import net.arizalsaputro.konsen.App;
import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.model.Person;
import net.arizalsaputro.konsen.model.User;
import net.arizalsaputro.konsen.rest.KonsenResponse;
import net.arizalsaputro.konsen.rest.KonsenService;
import net.arizalsaputro.konsen.util.Constant;
import net.arizalsaputro.konsen.util.MySharedPreference;
import net.arizalsaputro.konsen.view.AppBaseInterface;

import org.json.JSONException;

import io.reactivex.MaybeObserver;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by muharizals on 07/04/2017.
 */

public class AccountViewModel extends BaseObservable{
    public Person person;

    private String result;
    private CompositeDisposable compositeDisposable;
    private App app;
    private KonsenService konsenService;
    private MySharedPreference preference;
    private Context context;


    public ObservableInt accountvisible,loginvisible;

    private AppBaseInterface mListener;


    public AccountViewModel(Context context,AppBaseInterface listener) {
        this.mListener = listener;
        this.context = context;
        app= App.create(context);
        preference = new MySharedPreference(context);
        konsenService = app.getKonsenService();
        compositeDisposable = new CompositeDisposable();
        accountvisible = new ObservableInt(View.GONE);
        loginvisible = new ObservableInt(View.VISIBLE);
        person = preference.getPerson();
        checkVisibility();
    }

    private void checkVisibility(){
        if(ReactiveFB.getSessionManager().isLoggedIn()){
            accountvisible.set(View.VISIBLE);
            loginvisible.set(View.GONE);
        }else{
            loginvisible.set(View.VISIBLE);
            accountvisible.set(View.GONE);
        }
    }

    public String getName() {
        return person.name;
    }

    public String getImage() {
        return person.image;
    }

    public String getRanking() {
        return String.valueOf(person.ranking);
    }

    public String getPoint() {
        return String.valueOf(person.point);
    }

    public String getEmail(){
        if(person.email == null){
            return "email";
        }
        if(person.email.equals("")){
            return "email";
        }
        return person.email;
    }

    public void onBtnLoginClick(View view){
        dologinFacebook((Activity)view.getContext());
    }

    public void onBtnLogoutClick(View view){
        LoginManager.getInstance().logOut();
        checkVisibility();
    }

    private void dologinFacebook(Activity context){
        ReactiveLogin.login(context).subscribe(new MaybeObserver<LoginResult>() {
            @Override
            public void onSubscribe(Disposable d) {
                Log.d("loginFacebook","onsubscribe");
            }

            @Override
            public void onSuccess(LoginResult value) {
                result += "\n";
                result += "\ntoken = " + value.getAccessToken().getToken() ;
                Log.d("token",result);
                getProfile();
            }

            @Override
            public void onError(Throwable e) {
                Log.d("loginFacebook","gagal");
                Toast.makeText(context, context.getString(R.string.failed_login_facebook), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onComplete() {
                Log.d("loginFacebook","berhasil");
            }
        });
    }

    private void getProfile(){
        String fields = "picture.width(147).height(147),name,email";
        ReactiveRequest
                .getMe(fields)
                .map(this::parseUser)
                .subscribe(new SingleObserver<User>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d("Accounttes","onSubscribe");
                    }

                    @Override
                    public void onSuccess(User value) {
                        Log.d("Accounttes","onSuccess");
                        createOrUpdateNewUser(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("Accounttes","onError");
                    }
                });

    }

    private void handleResponse(KonsenResponse<Person> response){
        mListener.update();
        preference.setPerson(response.getData());
        person = response.getData();
        checkVisibility();
        notifyChange();
    }
    private void handleError(Throwable error){
        mListener.update();
        Toast.makeText(context, context.getString(R.string.failed_data), Toast.LENGTH_SHORT).show();
    }


    private void createOrUpdateNewUser(User user){
        unSubscribeFromObservable();
      compositeDisposable.add(konsenService.createNewUser(user.getId(),user.getName(),user.getPicture().getData().getUrl(),user.getEmail())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(app.subscribeScheduler())
        .subscribe(this::handleResponse,this::handleError)
      );

    }

    public void updateUserProfile(){
        unSubscribeFromObservable();
        compositeDisposable.add(
          konsenService.getUserData(preference.getStringProperty(Constant.USER_ID),null,null)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(app.subscribeScheduler())
                .subscribe(this::handleResponse,this::handleError)
        );
    }

    private User parseUser(GraphResponse response){
        Gson gson = new Gson();
        String data = null;
        try{
            data = response.getJSONObject().has("data") ?
                    response.getJSONObject().get("data").toString():
                    response.getJSONObject().toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return gson.fromJson(data,User.class);
    }


    @BindingAdapter("imageUrl") public static void setImageUrl(ImageView imageView, String url) {
        Glide.with(imageView.getContext()).load(url)
                .error(R.mipmap.ic_launcher)
                .into(imageView);
    }

    private void unSubscribeFromObservable(){
        if(compositeDisposable!=null && !compositeDisposable.isDisposed()){
            compositeDisposable.clear();
        }
    }

    public void reset(){
        unSubscribeFromObservable();
    }

}
