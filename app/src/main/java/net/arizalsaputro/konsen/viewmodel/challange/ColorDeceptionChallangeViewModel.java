package net.arizalsaputro.konsen.viewmodel.challange;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.helper.BeepHelper;
import net.arizalsaputro.konsen.logic.ColorDeceptionGameLogic;
import net.arizalsaputro.konsen.logic.GameLogicInterface;

/**
 * Created by muharizals on 10/04/2017.
 */

public class ColorDeceptionChallangeViewModel implements GameLogicInterface{
    private BeepHelper beepHelper;
    private ColorDeceptionGameLogic logic;

    public ObservableBoolean colorlayout;

    public ObservableField<String> txt1,txt2;
    public ObservableInt txt1_color,txt2_color;
    public ObservableInt bg1,bg2;

    public ColorDeceptionChallangeViewModel(Context context) {
        beepHelper = new BeepHelper(context);
        logic = new ColorDeceptionGameLogic(this,context);

        colorlayout = new ObservableBoolean(false);

        txt1 = new ObservableField<>();
        txt2 = new ObservableField<>();
        txt1_color = new ObservableInt(R.color.color_black);
        txt2_color = new ObservableInt(R.color.color_black);
        bg1 = new ObservableInt(R.color.cloud);
        bg2 = new ObservableInt(R.color.cloud);
    }

    public void start(){
        changeQuestions();
    }

    private void changeQuestions() {
            logic.doLogic();
            logic.questioncount++;
    }

    public void color1Click(View view){
        if(logic.checkAnswer(1)){
            beepHelper.success();
            logic.correct++;
            changeQuestions();
            ChallangeViewModel.that.correctAnser();
        }else{
            beepHelper.warning();
            logic.incorrect++;
            ChallangeViewModel.that.wrongAnswer();
        }

    }

    public void color2Click(View view){
        if(logic.checkAnswer(2)){
            beepHelper.success();
            logic.correct++;
            changeQuestions();
            ChallangeViewModel.that.correctAnser();
        }else{
            ChallangeViewModel.that.wrongAnswer();
            beepHelper.warning();
            logic.incorrect++;
        }
    }


    @Override
    public void update() {
        colorlayout.set(false);
        bg1.set(logic.getIntColorAnswer(1));
        txt1.set(logic.getStringColorAnswer(1));
        txt1_color.set(logic.getTextColor(bg1.get()));

        bg2.set(logic.getIntColorAnswer(2));
        txt2.set(logic.getStringColorAnswer(2));
        txt2_color.set(logic.getTextColor(bg2.get()));
        colorlayout.set(true);
    }

    @BindingAdapter("textColor")
    public static void setTextColor(Button view, int colorid){
        view.setTextColor(ContextCompat.getColor(view.getContext(),colorid));
    }

    @BindingAdapter("bgColor")
    public static void setBgColor(Button view, int colorid){
        view.setBackgroundColor(ContextCompat.getColor(view.getContext(),colorid));
    }

    @BindingAdapter("fadeVisible")
    public static void setFadeVisible(final View view, boolean visible) {
        if (view.getTag() == null) {
            view.setTag(true);
            view.setVisibility(visible ? View.VISIBLE : View.GONE);
        } else {
            view.animate().cancel();


            if (visible) {
                view.setVisibility(View.VISIBLE);
                view.setAlpha(0);
                view.animate().alpha(1).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setAlpha(1);
                    }
                });
            } else {
                view.animate().alpha(0).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setAlpha(1);
                        view.setVisibility(View.GONE);
                    }
                });
            }
        }
    }
}
