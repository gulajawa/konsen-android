package net.arizalsaputro.konsen.viewmodel.challange;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.view.View;

import net.arizalsaputro.konsen.helper.BeepHelper;
import net.arizalsaputro.konsen.logic.GameLogicInterface;
import net.arizalsaputro.konsen.logic.OperationGameLogic;

/**
 * Created by muharizals on 10/04/2017.
 */

public class OperationChallangeViewModel implements GameLogicInterface {
    public ObservableBoolean linear;
    public ObservableField<String> number_1,number_2,number_3;

    private OperationGameLogic logic;
    private BeepHelper beepHelper;

    public OperationChallangeViewModel(Context context){
        linear = new ObservableBoolean(false);
        number_1 = new ObservableField<>("0");
        number_2 = new ObservableField<>("0");
        number_3 = new ObservableField<>("1");

        logic = new OperationGameLogic(this);

        beepHelper = new BeepHelper(context);

    }

    @BindingAdapter("fadeVisible")
    public static void setFadeVisible(final View view, boolean visible) {
        if (view.getTag() == null) {
            view.setTag(true);
            view.setVisibility(visible ? View.VISIBLE : View.GONE);
        } else {
            view.animate().cancel();


            if (visible) {
                view.setVisibility(View.VISIBLE);
                view.setAlpha(0);
                view.animate().alpha(1).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setAlpha(1);
                    }
                });
            } else {
                view.animate().alpha(0).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setAlpha(1);
                        view.setVisibility(View.GONE);
                    }
                });
            }
        }
    }

    public void start(){
        changeQuestions();
    }

    private void changeQuestions(){
        logic.doLogic();
    }


    public void onAddClick(View view){
        if(logic.checkAnswer(1)){
            ChallangeViewModel.that.correctAnser();
            beepHelper.success();
            changeQuestions();
        }else{
            ChallangeViewModel.that.wrongAnswer();
            beepHelper.warning();
        }

    }

    public void onMinusClick(View view){
        if(logic.checkAnswer(2)){
            ChallangeViewModel.that.correctAnser();
            beepHelper.success();
            changeQuestions();
        }else{
            ChallangeViewModel.that.wrongAnswer();
            beepHelper.warning();
        }

    }

    public void onMultiClick(View view){
        if(logic.checkAnswer(3)){
            ChallangeViewModel.that.correctAnser();
            beepHelper.success();
            changeQuestions();
        }else{
            ChallangeViewModel.that.wrongAnswer();
            beepHelper.warning();
        }


    }
    public void onDevideClick(View view){
        if(logic.checkAnswer(4)){
            ChallangeViewModel.that.correctAnser();
            beepHelper.success();
            changeQuestions();
        }else{
            ChallangeViewModel.that.wrongAnswer();
            beepHelper.warning();
        }

    }

    @Override
    public void update() {
        linear.set(false);
        number_1.set(String.valueOf(logic.num_1));
        number_2.set(String.valueOf(logic.num_2));
        number_3.set(String.valueOf(logic.num_3));
        linear.set(true);

    }
}
