package net.arizalsaputro.konsen.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.model.Person;

/**
 * Created by muharizals on 06/04/2017.
 */

public class ItemPersonVewModel extends BaseObservable{

    private Person person;

    public ItemPersonVewModel(Person person) {
        this.person = person;
    }

    public String getName() {
        return person.name;
    }

    public String getImage() {
        return person.image;
    }

    public String getRanking() {
        return String.valueOf(person.ranking);
    }

    public String getPoint() {
        return String.valueOf(person.point);
    }

    public void setPerson(Person person){
        this.person = person;
        notifyChange();
    }

    @BindingAdapter("imageUrl") public static void setImageUrl(ImageView imageView, String url) {
        Glide.with(imageView.getContext()).load(url)
                .error(R.mipmap.ic_launcher)
                .override(60,60)
                .fitCenter()
                .into(imageView);
    }
}
