package net.arizalsaputro.konsen.viewmodel.game;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableFloat;
import android.databinding.ObservableInt;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.helper.BeepHelper;
import net.arizalsaputro.konsen.helper.SkillHelper;
import net.arizalsaputro.konsen.logic.BirdWatchingGameLogic;
import net.arizalsaputro.konsen.logic.GameLogicInterface;
import net.arizalsaputro.konsen.util.Constant;
import net.arizalsaputro.konsen.view.AppBaseInterface;

/**
 * Created by muharizals on 05/04/2017.
 */

public class BirdWatchingViewModel extends BaseObservable implements GameLogicInterface{

    private Context context;
    private BeepHelper beepHelper;
    private CountDownTimer timer;
    private SkillHelper skillHelper;
    private BirdWatchingGameLogic logic;
    private AppBaseInterface mListener;

    public ObservableBoolean showresult,showgame;
    public ObservableField<String> correct,incorrect;
    public ObservableFloat accuracy;
    public ObservableInt timesup;
    public ObservableField<String> textCounter;
    public ObservableField<String> textwarning;
    public ObservableInt startlayout;

    public ObservableArrayList<Integer> mybackgroundlist;

    public ObservableBoolean colorlayout;

    private boolean istimerrrunning = false;

    public BirdWatchingViewModel(Context c,AppBaseInterface listener){
        this.context = c;
        this.mListener = listener;
        beepHelper = new BeepHelper(context);
        skillHelper = new SkillHelper(context);

        logic = new BirdWatchingGameLogic(this,context);

        mybackgroundlist = new ObservableArrayList<>();
        initBgBtn();

        showresult = new ObservableBoolean(false);
        showgame = new ObservableBoolean(true);
        correct = new ObservableField<>(String.valueOf(logic.correct));
        incorrect = new ObservableField<>(String.valueOf(logic.incorrect));
        accuracy = new ObservableFloat(logic.getAccuracy());
        timesup = new ObservableInt(View.GONE);
        textCounter = new ObservableField<>(String.valueOf(Constant.COUNTER_LENGTH));
        textwarning = new ObservableField<>(context.getString(R.string.times_up));
        startlayout = new ObservableInt(View.VISIBLE);

        colorlayout = new ObservableBoolean(false);

        timer = new CountDownTimer(Constant.COUNTER_LENGTH*1000,1000) {
            @Override
            public void onTick(long l) {
                istimerrrunning = true;
                textCounter.set(String.valueOf(l/1000));
                if((l/1000) <= 5){
                    beepHelper.alarm();
                }
            }

            @Override
            public void onFinish() {
                istimerrrunning = false;
                textCounter.set(String.valueOf(0));
                colorlayout.set(false);
                timesup.set(View.VISIBLE);
                beepHelper.alarmLong();

                if(logic.correct >= Constant.COUNTER_LENGTH){
                    textwarning.set(context.getString(R.string.finish));
                }

                mListener.setToolbar();
                showResultGame();
                skillHelper.calculate(Constant.COUNTER_LENGTH,logic.questioncount,logic.correct,logic.incorrect);
            }
        };
    }

    private void initBgBtn(){
        for (int i=0;i<9;i++){
            mybackgroundlist.add(R.color.cloud);
        }
    }

    private void showResultGame(){
        new CountDownTimer(1500,1000){

            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                showgame.set(false);
                timesup.set(View.GONE);
                showresult.set(true);
                correct.set(String.valueOf(logic.correct));
                incorrect.set(String.valueOf(logic.incorrect));
                accuracy.set(logic.getAccuracy());
            }
        }.start();
    }



    public void onStartClick(View view){
        beepHelper.click();
        logic.reset();
        colorlayout.set(true);
        startlayout.set(View.GONE);
        showresult.set(false);
        showgame.set(true);
        istimerrrunning = true;
        textCounter.set(String.valueOf(Constant.COUNTER_LENGTH));
        timer.start();
        changeQuestions();
    }

    private void changeQuestions() {
        if(istimerrrunning){
            logic.questioncount++;
            logic.doLogic();
        }
    }

    public void onColorClick(View view){
        Button btn = (Button)view;
        int selected_color = (int)btn.getTag();
        System.out.println(String.valueOf(selected_color));
        if(logic.checkAnswer(selected_color)){
            beepHelper.success();
            logic.correct++;
            changeQuestions();
        }else{
            beepHelper.warning();
            logic.incorrect++;
        }

    }

    public void cancel(){
        timer.cancel();
    }

    @Override
    public void update() {
        colorlayout.set(false);
        for (int i=0;i<logic.getList().size();i++){
          mybackgroundlist.set(i,logic.getList().get(i));
        }
        colorlayout.set(true);

        if(timesup.get() == View.VISIBLE){
            colorlayout.set(false);
        }
    }

    @BindingAdapter("textColor")
    public static void setTextColor(Button view, int colorid){
        view.setTextColor(ContextCompat.getColor(view.getContext(),colorid));
    }

    @BindingAdapter("textHidden")
    public static void setTextHidden(Button v,int colorid){
        v.setTag(colorid);
    }

    @BindingAdapter("bgColor")
    public static void setBgColor(Button view, int colorid){
        view.setBackgroundColor(ContextCompat.getColor(view.getContext(),colorid));
    }

    @BindingAdapter("fadeVisible")
    public static void setFadeVisible(final View view, boolean visible) {
        if (view.getTag() == null) {
            view.setTag(true);
            view.setVisibility(visible ? View.VISIBLE : View.GONE);
        } else {
            view.animate().cancel();


            if (visible) {
                view.setVisibility(View.VISIBLE);
                view.setAlpha(0);
                view.animate().alpha(1).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setAlpha(1);
                    }
                });
            } else {
                view.animate().alpha(0).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setAlpha(1);
                        view.setVisibility(View.GONE);
                    }
                });
            }
        }
    }
}
