package net.arizalsaputro.konsen.viewmodel.challange;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableDouble;
import android.databinding.ObservableField;
import android.databinding.ObservableFloat;
import android.databinding.ObservableInt;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import net.arizalsaputro.konsen.BR;
import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.helper.BeepHelper;
import net.arizalsaputro.konsen.model.NameValuePair;
import net.arizalsaputro.konsen.model.Person;
import net.arizalsaputro.konsen.util.Constant;
import net.arizalsaputro.konsen.util.MySharedPreference;
import net.arizalsaputro.konsen.util.MyUtil;
import net.arizalsaputro.konsen.view.ChallangeInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by muharizals on 09/04/2017.
 */

public class ChallangeViewModel extends BaseObservable{

    public static ChallangeViewModel that;


    public ObservableBoolean onsearching,showMatchBox,showlayoutgame,showconnecting,showselectgame;
    public ObservableField<String> textCounter,scoreme,score_enemy,textresult;
    public ObservableInt timesup,fragment_layout;

    public Person me;

    @Bindable
    public Person enemy;

    private Context context;
    private MySharedPreference preference;
    private ChallangeInterface mListener;
    private BeepHelper beepHelper;

    private Socket socket;
    private String gameroom;

    public boolean haveOpponent,isdone,isplayer1;

    private CountDownTimer timergetopponent,gametimer,starttimer,changelayouttimer;

    private int mycurrent_score = 0,random_game=-1,gamelength;

    public ObservableFloat weight_me,wight_enemy;



    public ChallangeViewModel(Context context, ChallangeInterface mListener,int gamelength) {
        that = this;
        this.context = context;
        this.mListener = mListener;
        this.gamelength = gamelength;
        preference = new MySharedPreference(context);
        beepHelper = new BeepHelper(context);
        me = preference.getPerson();
        enemy = preference.getPerson();
        haveOpponent = false;
        isdone = false;

        //ini observable here
        showMatchBox = new ObservableBoolean(false);
        onsearching = new ObservableBoolean(false);
        showlayoutgame = new ObservableBoolean(false);
        showconnecting = new ObservableBoolean(true);
        showselectgame = new ObservableBoolean(false);

        weight_me = new ObservableFloat((float)1);
        wight_enemy = new ObservableFloat((float)1);

        timesup = new ObservableInt(View.GONE);
        fragment_layout = new ObservableInt(View.GONE);

        textCounter = new ObservableField<>("6");
        scoreme = new ObservableField<>("0");
        score_enemy = new ObservableField<>("0");
        textresult = new ObservableField<>(context.getString(R.string.you_loose));

        gametimer = new CountDownTimer(Constant.COUNTER_LENGTH*1000,1000) {
            @Override
            public void onTick(long l) {
                if((l/1000) <= 5){
                    beepHelper.alarm();
                }
                textCounter.set(String.valueOf(l/1000));
            }

            @Override
            public void onFinish() {
                isdone = true;
                textCounter.set("0");
                beepHelper.alarmLong();
                timesup.set(View.VISIBLE);
                fragment_layout.set(View.GONE);
                int mine = Integer.valueOf(scoreme.get());
                int ens = Integer.valueOf(score_enemy.get());
                double usrpoint = preference.getDoubleProperty(Constant.USER_POINT);
                if(mine > ens){
                    textresult.set(context.getString(R.string.you_win));
                    usrpoint = usrpoint + 10;
                }
                else if(mine == ens){
                    textresult.set(context.getString(R.string.draw));
                    usrpoint = usrpoint + 5;
                }
                else if(ens > mine){
                    usrpoint = usrpoint - 1;
                    if(usrpoint <= 0){
                        usrpoint = 0;
                    }
                    textresult.set(context.getString(R.string.you_loose));
                }
                updateMyPoint(usrpoint);
                preference.setDoubleProperty(Constant.USER_POINT,usrpoint);
            }
        };

        starttimer = new CountDownTimer(6000,1000) {
            @Override
            public void onTick(long l) {
                beepHelper.alarm();
                textCounter.set(String.valueOf(l/1000));
                try{
                    if(isplayer1){
                        if(random_game == -1){
                            random_game = MyUtil.getRandom(0,gamelength);
                            JSONObject obj = new JSONObject();
                            obj.put("code",Constant.CODE_GAME_RANDOM);
                            obj.put("data",random_game);
                            sendMessage(obj);
                        }
                    }
                }catch (JSONException e){

                }
            }

            @Override
            public void onFinish() {
                textCounter.set("0");
                beepHelper.alarmLong();
                if(random_game == -1){
                    random_game = MyUtil.getRandom(0,gamelength);
                }
                showselectgame.set(false);
                fragment_layout.set(View.VISIBLE);
                mListener.start();
                textCounter.set("20");
                gametimer.start();
            }
        };

         changelayouttimer =new CountDownTimer(500,1000){

            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                showlayoutgame.set(true);
                showMatchBox.set(false);
                if(isplayer1){
                    showselectgame.set(true);
                }else{
                    showselectgame.set(false);
                }
                starttimer.start();
            }
        };

        try {
            initialSocketIO();
            myLoopGetOpponent();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public int getGamePosition(){
        return random_game;
    }

    public void selectGameClick(View view){
        if(isplayer1){
            switch (view.getId()){
                case R.id.game_operation:
                    random_game = 0;
                    Toast.makeText(context, context.getString(R.string.operation_selected), Toast.LENGTH_SHORT).show();
                    break;
                case R.id.game_color_deception:
                    random_game = 2;
                    Toast.makeText(context, context.getString(R.string.color_deception_selected), Toast.LENGTH_SHORT).show();
                    break;
                case R.id.game_bird_eye:
                    random_game = 1;
                    Toast.makeText(context, context.getString(R.string.bird_eye_selected), Toast.LENGTH_SHORT).show();
                    break;
            }

            try{
                if(isplayer1){
                    if(random_game == -1){
                        random_game = MyUtil.getRandom(0,gamelength);
                    }
                    JSONObject obj = new JSONObject();
                    obj.put("code",Constant.CODE_GAME_RANDOM);
                    obj.put("data",random_game);
                    sendMessage(obj);
                }
            }catch (JSONException e){

            }
        }
    }

    private void myLoopGetOpponent(){
        timergetopponent = new CountDownTimer(Constant.LONG_TIME_GET_OPPONET_SECOND*1000,1000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                Log.d("myLoop","satu");
                if(!haveOpponent){
                    refreshFindOpponent();
                    myLoopGetOpponent();
                }
            }
        }.start();
    }


    private void initialSocketIO() throws URISyntaxException {
        IO.Options options = new IO.Options();
        String myid = preference.getStringProperty(Constant.USER_ID);
        options.query = "id=" + myid;
        socket = IO.socket(Constant.BASE_URL,options);
        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d("socket","connected");
                showMatchBox.set(true);
                onsearching.set(true);
                showconnecting.set(false);
            }
        });

        socket.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d("socket","disconnect");
            }
        });

        socket.on(Socket.EVENT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.d("socket","error");
            }
        });

       socket.on(Constant.EVENT_GAMEROOM_RECEIVER, new Emitter.Listener() {
           @Override
           public void call(Object... args) {
               try {
                   Log.d("socket","game room receiver");
                   JSONObject obj = (JSONObject)args[0];
                   if (obj.getString("player1").equals(myid) || obj.getString("player2").equals(myid)){
                            if(obj.getString("player2").equals(myid)){
                                isplayer1 = true;
                            }
                            gameroom = obj.getString("gameroom");
                            haveOpponent=true;
                            joinRoom();
                   }
               } catch (JSONException e) {
                   e.printStackTrace();
               }
           }
       });

        socket.on(Constant.EVENT_MESSAGE, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try{
                    Log.d("socket","message");
                    JSONObject obj = (JSONObject)args[0];
                    receiveMessage(obj);
                }catch (JSONException e){

                }
            }
        });

        socket.connect();
    }

    private void joinRoom() throws JSONException {
        JSONObject object = new JSONObject();
        object.put("gameroom",gameroom);
        socket.emit(Constant.EVENT_JOIN_ROOM,object);
    }


    private void leaveRoom() throws JSONException {
        if(!haveOpponent){
            return;
        }

        JSONObject object = new JSONObject();
        object.put("gameroom",gameroom);
        object.put("id",preference.getStringProperty(Constant.USER_ID));
        socket.emit(Constant.EVENT_LEAVE_ROOM,object);
    }

    private void refreshFindOpponent(){
        socket.emit(Constant.EVENT_REFRESH_FIND);
    }

    private void updateMyPoint(double  usrpoint){
        try {
            JSONObject obj = new JSONObject();
            String id = preference.getStringProperty(Constant.USER_ID);
            obj.put("id", id);
            obj.put("point", usrpoint);
            socket.emit(Constant.EVENT_UPDATE_POINT, obj);
        }catch (JSONException e){

        }
    }

    private void sendMessage(JSONObject object) throws JSONException {
        object.put("gameroom",gameroom);
        socket.emit(Constant.EVENT_SEND_MESSAGE,object);
    }

    private void receiveMessage(JSONObject obj) throws JSONException {
        String code = obj.getString("code");
        Gson gson = new Gson();
        switch (code){
            case Constant.CODE_START_GAME:
                JSONObject object = new JSONObject();
                object.put("code",Constant.CODE_MY_PROFILE);
                JSONObject data = new JSONObject(gson.toJson(me));
                object.put("data",data);
                sendMessage(object);
                break;

            case Constant.CODE_MY_PROFILE:
                String jsonString = gson.toJson(obj.get("data"));
                NameValuePair toconvert = gson.fromJson(jsonString,NameValuePair.class);
                enemy = toconvert.getData();
                notifyPropertyChanged(BR.enemy);
                onsearching.set(false);

                ((Activity)context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        changelayouttimer.start();
                    }
                });

                break;

            case Constant.CODE_GAME_RANDOM:
                random_game = obj.getInt("data");
                break;

            case Constant.CODE_LEAVE_ROOM:
                try{
                    changelayouttimer.cancel();
                    starttimer.cancel();
                    gametimer.cancel();

                }catch (Exception e){

                }
                if(!isdone){
                    mListener.surrender();
                }
                break;
            case Constant.CODE_MY_SCORE:
                int enemyScore = obj.getInt("score");
                score_enemy.set(String.valueOf(enemyScore));
                updateEnemyLinear();
                break;
        }
    }

    private void updateToEnemyMyScore(){
        try {
            JSONObject obj = new JSONObject();
            obj.put("code",Constant.CODE_MY_SCORE);
            obj.put("score",mycurrent_score);
            sendMessage(obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updateMyLinear(){
        if(mycurrent_score <= 0){
            return;
        }
        try{

            weight_me.set((float)mycurrent_score);

        }catch (Exception e){

        }
    }

    private void updateEnemyLinear(){
        int en_score = Integer.valueOf(score_enemy.get());
        if(en_score <= 0){
            return;
        }
        try{

            wight_enemy.set((float)en_score);
        }catch (Exception e){

        }

    }


    public void wrongAnswer(){
        if(mycurrent_score <= 0 ||  (mycurrent_score - 5) <= 0){
            mycurrent_score = 0;
        }else{
            mycurrent_score = mycurrent_score - 5;
            scoreme.set(String.valueOf(mycurrent_score));
        }
        updateToEnemyMyScore();
        updateMyLinear();
    }

    public void correctAnser(){
        mycurrent_score = mycurrent_score + 10;
        scoreme.set(String.valueOf(mycurrent_score));
        updateToEnemyMyScore();
        updateMyLinear();
    }

    @BindingAdapter("imageUrl") public static void setImageUrl(ImageView imageView, String url) {
        Glide.with(imageView.getContext()).load(url)
                .error(R.mipmap.ic_launcher)
                .into(imageView);
    }

    @BindingAdapter("fadeVisible")
    public static void setFadeVisible(final View view, boolean visible) {
        if (view.getTag() == null) {
            view.setTag(true);
            view.setVisibility(visible ? View.VISIBLE : View.GONE);
        } else {
            view.animate().cancel();


            if (visible) {
                view.setVisibility(View.VISIBLE);
                view.setAlpha(0);
                view.animate().alpha(1).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setAlpha(1);
                    }
                });
            } else {
                view.animate().alpha(0).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setAlpha(1);
                        view.setVisibility(View.GONE);
                    }
                });
            }
        }
    }

    public void reset(){
        if(timergetopponent != null){
            timergetopponent.cancel();
        }
        if(socket != null){
            if(socket.connected()){
                try {
                    leaveRoom();
                } catch (JSONException e) {
                    e.printStackTrace();
                }catch (Exception e){

                }
                try{
                    changelayouttimer.cancel();
                    starttimer.cancel();
                    gametimer.cancel();
                }catch (Exception e){

                }
                socket.close();
                socket.disconnect();
            }
        }
    }

    @BindingAdapter("myWeight")
    public static void setMyWeight(View view, float height) {
        System.out.println("tes");
        LinearLayout ll = (LinearLayout)view;
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0,100);
        lp.weight = height;
        ll.setLayoutParams(lp);
    }

}
