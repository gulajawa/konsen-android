package net.arizalsaputro.konsen.viewmodel.game;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableFloat;
import android.databinding.ObservableInt;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.helper.BeepHelper;
import net.arizalsaputro.konsen.helper.SkillHelper;
import net.arizalsaputro.konsen.logic.FollowLeaderLogic;
import net.arizalsaputro.konsen.logic.GameLogicInterface;
import net.arizalsaputro.konsen.model.LeaderGame;
import net.arizalsaputro.konsen.util.Constant;
import net.arizalsaputro.konsen.view.AppBaseInterface;

/**
 * Created by muharizals on 06/04/2017.
 */

public class FollowLeaderViewModel implements GameLogicInterface{

    private Context context;
    private BeepHelper beepHelper;
    private CountDownTimer timer;
    private SkillHelper skillHelper;
    private FollowLeaderLogic logic;
    private AppBaseInterface mListener;


    public ObservableBoolean showresult,showgame;
    public ObservableField<String> correct,incorrect;
    public ObservableFloat accuracy;
    public ObservableInt timesup;
    public ObservableField<String> textCounter;
    public ObservableField<String> textwarning;
    public ObservableInt startlayout;


    public ObservableInt colorlayout;

    private boolean istimerrrunning = false;

    private int counter,taillong;

    public LeaderGame ld1,ld2,ld3,ld4,ld5,ld6,ld7,ld8,ld9;

    public FollowLeaderViewModel(Context c, AppBaseInterface listener) {
        this.context = c;
        this.mListener = listener;
        beepHelper = new BeepHelper(context);
        skillHelper = new SkillHelper(context);
        logic = new FollowLeaderLogic(this);


        initBgBtn();


        showresult = new ObservableBoolean(false);
        showgame = new ObservableBoolean(true);
        correct = new ObservableField<>(String.valueOf(logic.correct));
        incorrect = new ObservableField<>(String.valueOf(logic.incorrect));
        accuracy = new ObservableFloat(logic.getAccuracy());
        timesup = new ObservableInt(View.GONE);
        textCounter = new ObservableField<>(String.valueOf(Constant.COUNTER_LENGTH));
        textwarning = new ObservableField<>(context.getString(R.string.times_up));
        startlayout = new ObservableInt(View.VISIBLE);

        colorlayout = new ObservableInt(View.GONE);


        timer = new CountDownTimer(Constant.COUNTER_LENGTH*1000,1000) {
            @Override
            public void onTick(long l) {
                istimerrrunning = true;
                textCounter.set(String.valueOf(l/1000));
                if((l/1000) <= 5){
                    beepHelper.alarm();
                }
            }

            @Override
            public void onFinish() {
                istimerrrunning = false;
                textCounter.set(String.valueOf(0));
                colorlayout.set(View.GONE);
                timesup.set(View.VISIBLE);
                beepHelper.alarmLong();

                if(logic.correct >= Constant.COUNTER_LENGTH){
                    textwarning.set(context.getString(R.string.finish));
                }

                mListener.setToolbar();
                showResultGame();
                skillHelper.calculate(Constant.COUNTER_LENGTH,logic.questioncount,logic.correct,logic.incorrect);
            }
        };

    }

    private void initBgBtn(){
        ld1 = new LeaderGame(0,R.color.midnight_blue,false);
        ld2 = new LeaderGame(0,R.color.midnight_blue,false);
        ld3 = new LeaderGame(0,R.color.midnight_blue,false);
        ld4 = new LeaderGame(0,R.color.midnight_blue,false);
        ld5 = new LeaderGame(0,R.color.midnight_blue,false);
        ld6 = new LeaderGame(0,R.color.midnight_blue,false);
        ld7 = new LeaderGame(0,R.color.midnight_blue,false);
        ld8 = new LeaderGame(0,R.color.midnight_blue,false);
        ld9 = new LeaderGame(0,R.color.midnight_blue,false);
    }

    private void showResultGame(){
        new CountDownTimer(1500,1000){

            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                showgame.set(false);
                timesup.set(View.GONE);
                showresult.set(true);
                correct.set(String.valueOf(logic.correct));
                incorrect.set(String.valueOf(logic.incorrect));
                accuracy.set(logic.getAccuracy());
            }
        }.start();
    }



    public void onStartClick(View view){
        beepHelper.click();
        logic.reset();
        startlayout.set(View.GONE);
        showresult.set(false);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showgame.set(true);
                counter = 0;
                istimerrrunning = true;
                textCounter.set(String.valueOf(Constant.COUNTER_LENGTH));
                timer.start();
                changeQuestions();
            }
        },250);

    }

    private void changeQuestions() {
        if(istimerrrunning){
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    counter = 0;
                    taillong = 0;
                    logic.questioncount++;
                    logic.doLogic();
                }
            },250);
        }
    }

    public void onColorClick(View view){
        Button button = (Button)view;
        int tag = (int)button.getTag();
        if(tag == counter+1){
            beepHelper.success();
            counter++;
            logic.correct++;
            if(counter == taillong){
                changeQuestions();
            }else{
                view.setVisibility(View.INVISIBLE);
            }
        }else{
            logic.incorrect++;
            beepHelper.warning();
            changeQuestions();
        }

    }

    public void cancel(){
        timer.cancel();
    }

    private void setNullAndFalse(){
        ld1.setOrder(0);
        ld1.setInvisible(View.INVISIBLE);
        ld2.setOrder(0);
        ld2.setInvisible(View.INVISIBLE);
        ld3.setOrder(0);
        ld3.setInvisible(View.INVISIBLE);
        ld4.setOrder(0);
        ld4.setInvisible(View.INVISIBLE);
        ld5.setOrder(0);
        ld5.setInvisible(View.INVISIBLE);
        ld6.setOrder(0);
        ld6.setInvisible(View.INVISIBLE);
        ld7.setOrder(0);
        ld7.setInvisible(View.INVISIBLE);
        ld8.setOrder(0);
        ld8.setInvisible(View.INVISIBLE);
        ld9.setOrder(0);
        ld9.setInvisible(View.INVISIBLE);

    }

    @Override
    public void update() {
        colorlayout.set(View.VISIBLE);
        taillong = logic.answer_length;
        setNullAndFalse();

        Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                doRecurShow(0,1,false);
            }
        });
        if(timesup.get() == View.VISIBLE){
            colorlayout.set(View.GONE);
        }
    }


    private void doRecurShow(final int position, final int counter, boolean isdone){

        if(isdone){
            return;
        }

        if(position == logic.getList().size()){
            doRecurShow(0,counter,isdone);
            return;
        }
        LeaderGame tmp = logic.getList().get(position);
        if(tmp.getOrder() == counter){
           switch (position){
               case 0 :
                   if(ld1.getInvisible() == View.INVISIBLE){
                       ld1.setVisibility(true);
                       ld1.setOrder(tmp.getOrder());
                   }
                   break;
               case 1 :
                   if(ld2.getInvisible() == View.INVISIBLE){
                       ld2.setVisibility(true);
                       ld2.setOrder(tmp.getOrder());
                   }

                   break;
               case 2:
                   if(ld3.getInvisible() == View.INVISIBLE){
                       ld3.setVisibility(true);
                       ld3.setOrder(tmp.getOrder());
                   }
                   break;
               case 3:
                   if(ld4.getInvisible() == View.INVISIBLE){
                       ld4.setVisibility(true);
                       ld4.setOrder(tmp.getOrder());
                   }
                   break;
               case 4:
                   if(ld5.getInvisible() == View.INVISIBLE){
                       ld5.setVisibility(true);
                       ld5.setOrder(tmp.getOrder());
                   }
                   break;
               case 5:
                   if(ld6.getInvisible() == View.INVISIBLE){
                       ld6.setVisibility(true);
                       ld6.setOrder(tmp.getOrder());
                   }
                   break;
               case 6:
                   if(ld7.getInvisible() == View.INVISIBLE){
                       ld7.setVisibility(true);
                       ld7.setOrder(tmp.getOrder());
                   }
                   break;
               case 7:
                   if(ld8.getInvisible() == View.INVISIBLE){
                       ld8.setVisibility(true);
                       ld8.setOrder(tmp.getOrder());
                   }
                   break;
               case 8:
                   if(ld9.getInvisible() == View.INVISIBLE){
                       ld9.setVisibility(true);
                       ld9.setOrder(tmp.getOrder());
                   }
                   break;
           }

           if(counter == taillong){
               isdone = true;
           }

           final boolean finalIsDone = isdone;

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    doRecurShow(position+1,counter+1,finalIsDone);
                }
            },250);
        }else{
          doRecurShow(position+1,counter,isdone);
        }
    }


    @BindingAdapter("textColor")
    public static void setTextColor(Button view, int colorid){
        view.setTextColor(ContextCompat.getColor(view.getContext(),colorid));
    }

    @BindingAdapter("textHidden")
    public static void setTextHidden(Button v,int order){
        v.setTag(order);
    }

    @BindingAdapter("bgColor")
    public static void setBgColor(Button view, int colorid){
        view.setBackgroundColor(ContextCompat.getColor(view.getContext(),colorid));
    }

    @BindingAdapter("fadeVisible")
    public static void setFadeVisible(final View view, boolean visible) {
        if (view.getTag() == null) {
            view.setTag(true);
            view.setVisibility(visible ? View.VISIBLE : View.GONE);
        } else {
            view.animate().cancel();


            if (visible) {
                view.setVisibility(View.VISIBLE);
                view.setAlpha(0);
                view.animate().alpha(1).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setAlpha(1);
                    }
                });
            } else {
                view.animate().alpha(0).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setAlpha(1);
                        view.setVisibility(View.GONE);
                    }
                });
            }
        }
    }

    @BindingAdapter("fadeInVisible")
    public static void setFadeInVisible(final View view, boolean visible) {
        if (view.getTag() == null) {
            view.setTag(true);
            view.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
        } else {
            view.animate().cancel();


            if (visible) {
                view.setVisibility(View.VISIBLE);
                view.setAlpha(0);
                view.animate().alpha(1).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setAlpha(1);
                    }
                });
            } else {
                view.animate().alpha(0).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setAlpha(1);
                        view.setVisibility(View.INVISIBLE);
                    }
                });
            }
        }
    }

}
