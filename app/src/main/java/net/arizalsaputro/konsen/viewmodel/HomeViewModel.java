package net.arizalsaputro.konsen.viewmodel;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

import com.beltaief.reactivefb.ReactiveFB;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.helper.BeepHelper;
import net.arizalsaputro.konsen.helper.SkillHelper;
import net.arizalsaputro.konsen.view.ListPlayActivity;
import net.arizalsaputro.konsen.view.MainActivity;
import net.arizalsaputro.konsen.view.challange.ChallangeActivity;

/**
 * Created by muharizals on 03/04/2017.
 */

public class HomeViewModel {


    private BeepHelper beepHelper;

    public HomeViewModel(Context context) {
        beepHelper = new BeepHelper(context);
    }

    public void onPlayClick(View view){
        beepHelper.click();
        view.getContext().startActivity(ListPlayActivity.start(view.getContext()));
    }

    public void onChallangeClick(View view){
        if(ReactiveFB.getSessionManager().isLoggedIn()){
            beepHelper.click();
            view.getContext().startActivity(ChallangeActivity.start(view.getContext()));
        }else{
            MainActivity.that.binding.viewpager.setCurrentItem(2);
        }

    }
}

