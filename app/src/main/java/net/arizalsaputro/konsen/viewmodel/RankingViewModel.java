package net.arizalsaputro.konsen.viewmodel;

import android.content.Context;
import android.widget.Toast;

import net.arizalsaputro.konsen.App;
import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.model.Person;
import net.arizalsaputro.konsen.rest.KonsenResponse;
import net.arizalsaputro.konsen.rest.KonsenService;
import net.arizalsaputro.konsen.view.AppBaseInterface;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by muharizals on 06/04/2017.
 */

public class RankingViewModel {

    private CompositeDisposable compositeDisposable;
    private App app;
    private KonsenService konsenService;
    private ArrayList<Person> list;

    private Context context;
    private AppBaseInterface mListener;

    public RankingViewModel(Context context, AppBaseInterface mListener) {
        list = new ArrayList<>();
        this.context = context;
        this.mListener = mListener;
        compositeDisposable = new CompositeDisposable();
        app= App.create(context);
        konsenService = app.getKonsenService();
    }

    private void handleResponse(KonsenResponse<ArrayList<Person>> response){
        list = response.getData();
        mListener.update();

    }
    private void handleError(Throwable error){
        mListener.update();
        Toast.makeText(context, context.getString(R.string.failed_data), Toast.LENGTH_SHORT).show();
    }

    public ArrayList<Person> getList() {
        return list;
    }

    public void getRankingListData(){
        unSubscribeFromObservable();
        compositeDisposable.add(
          konsenService.fetchRanking()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(app.subscribeScheduler())
                .subscribe(this::handleResponse,this::handleError)
        );
    }

    private void unSubscribeFromObservable(){
        if(compositeDisposable!=null && !compositeDisposable.isDisposed()){
            compositeDisposable.clear();
        }
    }

    public void reset(){
        unSubscribeFromObservable();
    }

}
