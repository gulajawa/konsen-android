package net.arizalsaputro.konsen.viewmodel.game;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableFloat;
import android.databinding.ObservableInt;
import android.os.CountDownTimer;
import android.view.View;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.helper.BeepHelper;
import net.arizalsaputro.konsen.helper.SkillHelper;
import net.arizalsaputro.konsen.logic.OperationGameLogic;
import net.arizalsaputro.konsen.logic.GameLogicInterface;
import net.arizalsaputro.konsen.util.Constant;
import net.arizalsaputro.konsen.view.AppBaseInterface;

/**
 * Created by muharizals on 03/04/2017.
 */

public class OperationGameViewModel implements GameLogicInterface {
    public ObservableField<Integer> gamelayout;
    public ObservableField<Integer> startlayout;
    public ObservableBoolean linear;
    public ObservableInt timesup;
    public ObservableField<String> textCounter;
    public ObservableField<String> number_1,number_2,number_3;
    public ObservableField<String> textwarning;

    public ObservableBoolean showResult,showgame;

    private AppBaseInterface mListener;
    private SkillHelper skillHelper;

    private Context context;
    private CountDownTimer timer;
    private OperationGameLogic logic;
    private BeepHelper beepHelper;

    public ObservableField<String> correct,incorrext;
    public ObservableFloat accuracy;

    private boolean istimerrunning = false;

    private int questioncount = 0;

    private int correct_answer=0,wrong_answer=0;

    public OperationGameViewModel(final Context context, AppBaseInterface listener) {
        this.context = context;
        this.mListener = listener;
        skillHelper = new SkillHelper(context);
        beepHelper = new BeepHelper(context);
        logic = new OperationGameLogic(this);
        showResult = new ObservableBoolean(false);
        showgame = new ObservableBoolean(true);

        correct = new ObservableField<>(String.valueOf(correct_answer));
        incorrext = new ObservableField<>(String.valueOf(wrong_answer));


        gamelayout = new ObservableField<>(View.INVISIBLE);
        startlayout = new ObservableField<>(View.VISIBLE);
        linear = new ObservableBoolean(false);
        textCounter = new ObservableField<>(String.valueOf(Constant.COUNTER_LENGTH));
        textwarning = new ObservableField<>(context.getString(R.string.times_up));
        timesup = new ObservableInt(View.INVISIBLE);
        timer = new CountDownTimer(Constant.COUNTER_LENGTH*1000,1000){

            @Override
            public void onTick(long l) {
                istimerrunning = true;
                textCounter.set(String.valueOf(l/1000));
                if((l/1000) <= 5){
                    beepHelper.alarm();
                }
            }

            @Override
            public void onFinish() {
                istimerrunning = false;
                textCounter.set(String.valueOf(0));
                gamelayout.set(View.GONE);
                timesup.set(View.VISIBLE);
                linear.set(false);
                beepHelper.alarmLong();
                if(correct_answer>=Constant.COUNTER_LENGTH){
                    textwarning.set(context.getString(R.string.finish));
                }
                mListener.setToolbar();
                showResultGame();
                skillHelper.calculate(Constant.COUNTER_LENGTH,questioncount,correct_answer,wrong_answer);
            }
        };
        number_1 = new ObservableField<>(String.valueOf(logic.num_1));
        number_2 = new ObservableField<>(String.valueOf(logic.num_2));
        number_3 = new ObservableField<>(String.valueOf(logic.num_3));

        accuracy = new ObservableFloat(0.1F);
    }

    private void showResultGame(){
        new CountDownTimer(1500,1000){

            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                showgame.set(false);
                showResult.set(true);
                timesup.set(View.GONE);

                correct.set(String.valueOf(correct_answer));
                incorrext.set(String.valueOf(wrong_answer));
                accuracy.set(logic.getAccuracy(questioncount,correct_answer,wrong_answer));
            }
        }.start();
    }

    public void onStartClick(View view){
        beepHelper.click();
        gamelayout.set(View.VISIBLE);
        startlayout.set(View.GONE);
        showResult.set(false);
        showgame.set(true);
        correct_answer = 0;
        wrong_answer = 0;
        questioncount = 0;
        logic.resrtLvlcount();
        istimerrunning = true;
        textCounter.set(String.valueOf(Constant.COUNTER_LENGTH));
        timer.start();
        changeQuestions();
    }

    private void changeQuestions(){
        if(!istimerrunning){
            return;
        }
        questioncount++;
        logic.doLogic();
    }


    public void onAddClick(View view){
        if(logic.checkAnswer(1)){
            beepHelper.success();
            correct_answer++;
            changeQuestions();
        }else{
            beepHelper.warning();
            wrong_answer++;
        }

    }

    public void onMinusClick(View view){
        if(logic.checkAnswer(2)){
            beepHelper.success();
            correct_answer++;
            changeQuestions();
        }else{
            beepHelper.warning();
            wrong_answer++;
        }

    }

    public void onMultiClick(View view){
        if(logic.checkAnswer(3)){
            beepHelper.success();
            correct_answer++;
            changeQuestions();
        }else{
            beepHelper.warning();
            wrong_answer++;
        }


    }
    public void onDevideClick(View view){
        if(logic.checkAnswer(4)){
            beepHelper.success();
            correct_answer++;
            changeQuestions();
        }else{
            beepHelper.warning();
            wrong_answer++;
        }

    }


    @BindingAdapter("fadeVisible")
    public static void setFadeVisible(final View view, boolean visible) {
        if (view.getTag() == null) {
            view.setTag(true);
            view.setVisibility(visible ? View.VISIBLE : View.GONE);
        } else {
            view.animate().cancel();


            if (visible) {
                view.setVisibility(View.VISIBLE);
                view.setAlpha(0);
                view.animate().alpha(1).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setAlpha(1);
                    }
                });
            } else {
                view.animate().alpha(0).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setAlpha(1);
                        view.setVisibility(View.GONE);
                    }
                });
            }
        }
    }

    public void cancel(){
        timer.cancel();
    }


    @Override
    public void update() {
        linear.set(false);
        number_1.set(String.valueOf(logic.num_1));
        number_2.set(String.valueOf(logic.num_2));
        number_3.set(String.valueOf(logic.num_3));
        linear.set(true);

        if(gamelayout.get().equals(View.GONE)){
            linear.set(false);
        }
    }
}
