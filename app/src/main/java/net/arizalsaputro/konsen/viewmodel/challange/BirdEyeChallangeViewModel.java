package net.arizalsaputro.konsen.viewmodel.challange;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.helper.BeepHelper;
import net.arizalsaputro.konsen.logic.BirdWatchingGameLogic;
import net.arizalsaputro.konsen.logic.GameLogicInterface;

/**
 * Created by muharizals on 10/04/2017.
 */

public class BirdEyeChallangeViewModel implements GameLogicInterface {
    public ObservableBoolean colorlayout;
    private BirdWatchingGameLogic logic;
    private BeepHelper beepHelper;

    public ObservableArrayList<Integer> mybackgroundlist;



    public BirdEyeChallangeViewModel(Context context) {
        logic = new BirdWatchingGameLogic(this,context);
        beepHelper = new BeepHelper(context);

        colorlayout = new ObservableBoolean(false);
        mybackgroundlist = new ObservableArrayList<>();
        initBgBtn();

    }

    public void start()
    {
        changeQuestions();
    }
    private void initBgBtn(){
        for (int i=0;i<9;i++){
            mybackgroundlist.add(R.color.cloud);
        }
    }

    private void changeQuestions() {
            logic.questioncount++;
            logic.doLogic();
    }

    public void onColorClick(View view){
        Button btn = (Button)view;
        int selected_color = (int)btn.getTag();
        if(logic.checkAnswer(selected_color)){
            beepHelper.success();
            logic.correct++;
            ChallangeViewModel.that.correctAnser();
            changeQuestions();
        }else{
            beepHelper.warning();
            logic.incorrect++;
            ChallangeViewModel.that.wrongAnswer();
        }

    }


    @Override
    public void update() {
        colorlayout.set(false);
        for (int i=0;i<logic.getList().size();i++){
            mybackgroundlist.set(i,logic.getList().get(i));
        }
        colorlayout.set(true);

    }

    @BindingAdapter("textColor")
    public static void setTextColor(Button view, int colorid){
        view.setTextColor(ContextCompat.getColor(view.getContext(),colorid));
    }

    @BindingAdapter("textHidden")
    public static void setTextHidden(Button v,int colorid){
        v.setTag(colorid);
    }

    @BindingAdapter("bgColor")
    public static void setBgColor(Button view, int colorid){
        view.setBackgroundColor(ContextCompat.getColor(view.getContext(),colorid));
    }

    @BindingAdapter("fadeVisible")
    public static void setFadeVisible(final View view, boolean visible) {
        if (view.getTag() == null) {
            view.setTag(true);
            view.setVisibility(visible ? View.VISIBLE : View.GONE);
        } else {
            view.animate().cancel();


            if (visible) {
                view.setVisibility(View.VISIBLE);
                view.setAlpha(0);
                view.animate().alpha(1).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setAlpha(1);
                    }
                });
            } else {
                view.animate().alpha(0).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setAlpha(1);
                        view.setVisibility(View.GONE);
                    }
                });
            }
        }
    }
}
