package net.arizalsaputro.konsen.viewmodel;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.model.Game;
import net.arizalsaputro.konsen.view.MainActivity;
import net.arizalsaputro.konsen.view.game.BirdWatchtingActivity;
import net.arizalsaputro.konsen.view.game.ColorDeceptionGameActivity;
import net.arizalsaputro.konsen.view.game.FollowLeaderActivity;
import net.arizalsaputro.konsen.view.game.OperationGameActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by muharizals on 03/04/2017.
 */

public class ListPlayViewModel {
    private Context context;

    public ListPlayViewModel(Context context) {
        this.context = context;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public List<Game> getLisGame(){
        List<Game> list = new ArrayList<>();
        list.add(new Game(context.getString(R.string.game_opration),context.getDrawable(R.drawable.ic_operation), OperationGameActivity.class));
        list.add(new Game(context.getString(R.string.game_color_deception),context.getDrawable(R.drawable.ic_color_palette),ColorDeceptionGameActivity.class));
        list.add(new Game(context.getString(R.string.game_bird_eye),context.getDrawable(R.drawable.ic_owl), BirdWatchtingActivity.class));
        list.add(new Game(context.getString(R.string.game_follow_leader),context.getDrawable(R.drawable.ic_follow_leader), FollowLeaderActivity.class));

        //coming soon
        list.add(new Game(context.getString(R.string.game_coming_soon),context.getDrawable(R.mipmap.ic_launcher), null));

        return list;
    }
}
