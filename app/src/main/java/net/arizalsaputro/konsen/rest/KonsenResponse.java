package net.arizalsaputro.konsen.rest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by muharizals on 09/04/2017.
 */

public class KonsenResponse<T> {
    @SerializedName("error")
    private boolean error;

    @SerializedName("data")
    private T data;

    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public T getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }
}
