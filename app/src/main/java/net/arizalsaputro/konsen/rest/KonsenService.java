package net.arizalsaputro.konsen.rest;


import android.support.annotation.Nullable;

import net.arizalsaputro.konsen.model.Person;

import java.util.ArrayList;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;


/**
 * Created by muharizals on 09/04/2017.
 */

public interface KonsenService {
    @GET("ranking")
    Observable<KonsenResponse<ArrayList<Person>>> fetchRanking();

    @GET("user")
    Observable<KonsenResponse<Person>> getUserData(@Nullable @Query("id")String id,@Nullable @Query("fbid")String fbid,@Nullable @Query("email")String email);

    @FormUrlEncoded
    @POST("new")
    Observable<KonsenResponse<Person>> createNewUser(@Field("id")String id,@Field("name")String name,@Field("picture")String picture,@Nullable @Field("email")String email);

    @FormUrlEncoded
    @PUT("update")
    Observable<KonsenResponse<Person>> updatePoint(@Field("point")double point,@Nullable @Field("id")String id,@Nullable @Field("fbid")String fbid,@Nullable @Field("email")String email);

}
