package net.arizalsaputro.konsen.rest;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by muharizals on 09/04/2017.
 */

public class KonsenFactory {
    public static final String BASE_URL = "http://api.konsen.arizalsaputro.net/";

    public static KonsenService create(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return retrofit.create(KonsenService.class);
    }

}
