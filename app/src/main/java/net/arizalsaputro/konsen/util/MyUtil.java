package net.arizalsaputro.konsen.util;

import java.util.Random;

/**
 * Created by muharizals on 06/04/2017.
 */

public class MyUtil {
    public static float getAccuracy(int correct,int incorrect){
        try{
            double hsl1 = correct+incorrect;
            double hsl2 = (double)correct/hsl1;
            double hsl3 = hsl2*100;
            return (float)hsl3;
        }catch (Exception e){
            return 0.1F;
        }

    }

    public static float getSpeed(int correct,int length){
        try{
            double hsl1 = (double)correct/(double)length;
            double hsl2 = hsl1*100;
            return (float)hsl2;
        }catch (Exception e){
            return 0.1F;
        }
    }

    public static float getCalculation(int length,int correct){
        try{
            double hsl1 = 100d/(double)length;
            double hsl2 = correct*hsl1;
            return (float)hsl2;
        }catch (Exception e){
            return 0.1F;
        }
    }

    public static int getRandom(int min,int max){
        Random random = new Random();
        int result = 0;
        try {
            result= random.nextInt(max - min ) + min;
        }catch (IllegalArgumentException e){

        }catch (Exception e){

        }
        return result;
    }

}
