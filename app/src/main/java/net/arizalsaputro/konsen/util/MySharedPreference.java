package net.arizalsaputro.konsen.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import net.arizalsaputro.konsen.model.Person;


/**
 * Created by muharizals on 03/04/2017.
 */

public class MySharedPreference {
    public SharedPreferences preferences;

    public MySharedPreference(Context context){
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static MySharedPreference with(Context context){
        return new MySharedPreference(context);
    }

    public void setIntProperty(String key,int value){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public int getIntProperty(String key){
        return preferences.getInt(key,0);
    }

    public void setStringProperty(String key,String value){
        if(value == null){
            return;
        }
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getStringProperty(String key){
        return preferences.getString(key,"");
    }

    public void setDoubleProperty(String key,double value){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(key,Double.doubleToRawLongBits(value));
        editor.apply();
    }

    public double getDoubleProperty(String key){
        if( !preferences.contains(key))  return 0;
        return Double.longBitsToDouble(preferences.getLong(key, 0));
    }

    public void setFloatProperty(String key,float value){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat(key,value);
        editor.apply();
    }

    public float getFloatProperty(String key){
        return preferences.getFloat(key,0F);
    }

    public void setBoleanProperty(String key,boolean value){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key,value);
        editor.apply();
    }


    public boolean getBoleanProperty(String key){
        return preferences.getBoolean(key,true);
    }

    public boolean isMusicOn(){
        return this.getBoleanProperty(Constant.IS_MUSIC_ON);
    }

    public void setMusicOnOff(){
        boolean ismusicon = !this.isMusicOn();
        this.setBoleanProperty(Constant.IS_MUSIC_ON,ismusicon);
    }

    public boolean isShowDialog(String key){
        return this.getBoleanProperty(key);
    }

    public void dontShowDialog(String key){
        this.setBoleanProperty(key,false);
    }

    public void setPerson(Person person){
        this.setStringProperty(Constant.USER_NAME,person.name);
        this.setStringProperty(Constant.USER_EMAIL,person.email);
        this.setStringProperty(Constant.USER_FBID,person.fbid);
        this.setStringProperty(Constant.USER_ID,person.id);
        this.setStringProperty(Constant.USER_PICTURE,person.image);
        this.setIntProperty(Constant.USER_RANKING,person.ranking);
        this.setDoubleProperty(Constant.USER_POINT,person.point);
    }

    public Person getPerson(){
        String name = this.getStringProperty(Constant.USER_NAME);
        String email = this.getStringProperty(Constant.USER_EMAIL);
        String fbid = this.getStringProperty(Constant.USER_FBID);
        String id = this.getStringProperty(Constant.USER_ID);
        String picture  = this.getStringProperty(Constant.USER_PICTURE);
        int ranking = this.getIntProperty(Constant.USER_RANKING);
        double point= this.getDoubleProperty(Constant.USER_POINT);
        return new Person(id,fbid,ranking,name,picture,email,point);
    }
}

