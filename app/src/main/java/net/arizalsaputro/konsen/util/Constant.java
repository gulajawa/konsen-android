package net.arizalsaputro.konsen.util;

/**
 * Created by muharizals on 03/04/2017.
 */

public class Constant {
    public static final String IS_MUSIC_ON = "is_music_on?";

    public static final String BASE_URL = "http://api.konsen.arizalsaputro.net/";

    public static final String CODE_LEAVE_ROOM ="LEAVE_ROOM";
    public static final String CODE_START_GAME = "START_GAME";

    public static final String CODE_MY_PROFILE = "CODE_MY_PROFILE";
    public static final String CODE_MY_SCORE = "CODE_MY_SCORE";
    public static final String CODE_GAME_RANDOM = "CODE_GAME_RANDOM";

    public static final String EVENT_GAMEROOM_RECEIVER = "gameroomreceiver";
    public static final String EVENT_JOIN_ROOM = "join:room";
    public static final String EVENT_REFRESH_FIND = "refresh:find";
    public static final String EVENT_LEAVE_ROOM = "leave:room";
    public static final String EVENT_SEND_MESSAGE = "send:message";
    public static final String EVENT_MESSAGE = "message";
    public static final String EVENT_UPDATE_POINT = "update:point";

    public static final String SHOW_DIALOG_OPERATION = "operation_dialog";
    public static final String SHOW_DIALOG_COLOR_DECEPTION = "SHOW_DIALOG_COLOR_DECEPTION";
    public static final String SHOW_DIALOG_BIRD_WATCHING = "SHOW_DIALOG_BIRD_WATCHING";
    public static final String SHOW_DIALOG_FOLLOW_LEADER = "SHOW_DIALOG_FOLLOW_LEADER";

    public final static int MAX_VOLUME = 100;
    public static final int GRID_LIST_LENGTH = 3;
    public static final int COUNTER_LENGTH = 20;
    public static final int LONG_TIME_GET_OPPONET_SECOND = 30;

    public static final String SPEED = "speed";
    public static final String MEMMORY = "memmory";
    public static final String ACCURACY = "ACCURACY";
    public static final String CALCULATION = "CALCULATION";
    public static final String JUDGMENT = "JUDGMENT";
    public static final String OBSERVATION = "OBSERVATION";

    public static final String USER_NAME = "USER_NAME";
    public static final String USER_PICTURE = "USER_PICTURE";
    public static final String  USER_EMAIL = "USER_EMAIL";
    public static final String USER_POINT = "USER_POINT";
    public static final String USER_FBID = "USER_FBID";
    public static final String USER_RANKING = "USER_RANKING";
    public static final String USER_ID = "USER_ID";

}

