package net.arizalsaputro.konsen.helper;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Vibrator;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.util.MySharedPreference;

/**
 * Created by muharizals on 04/04/2017.
 */

public class BeepHelper implements MediaPlayer.OnCompletionListener{
    private Context context;
    private Handler handler;
    private MediaPlayer mediaPlayer = null;
    private MySharedPreference preference;
    private Vibrator vibrator;

    public BeepHelper(Context context) {
        this.context = context;
        handler = new Handler();
        preference = new MySharedPreference(context);
        vibrator = (Vibrator) context.getSystemService(context.VIBRATOR_SERVICE);
    }

    public static BeepHelper with(Context context){
        return new BeepHelper(context);
    }

    public void warning(){
        if(!preference.isMusicOn()){
            vibrator.vibrate(250);
            return;
        }
        try{
            if(mediaPlayer != null){
                if(mediaPlayer.isPlaying()){
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            }
        }catch (Exception e){

        }
       Runnable runbeep = new Runnable() {
           @Override
           public void run() {
               mediaPlayer = MediaPlayer.create(context, R.raw.wrong);
               mediaPlayer.setLooping(false);
               mediaPlayer.start();
               mediaPlayer.setOnCompletionListener(BeepHelper.this);
           }
       };
       handler.post(runbeep);
    }

    public void success(){
        if(!preference.isMusicOn()){
            return;
        }
        try{
            if(mediaPlayer != null){
                if(mediaPlayer.isPlaying()){
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            }
        }catch (Exception e){

        }
        Runnable runbeep = new Runnable() {
            @Override
            public void run() {
                mediaPlayer = MediaPlayer.create(context, R.raw.success);
                mediaPlayer.setLooping(false);
                mediaPlayer.start();
                mediaPlayer.setOnCompletionListener(BeepHelper.this);
            }
        };
        handler.post(runbeep);
    }

    public void click(){
        if(!preference.isMusicOn()){
            return;
        }
        try{
            if(mediaPlayer != null){
                if(mediaPlayer.isPlaying()){
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            }
        }catch (Exception e){

        }
        Runnable runbeep = new Runnable() {
            @Override
            public void run() {
                mediaPlayer = MediaPlayer.create(context, R.raw.button);
                mediaPlayer.setLooping(false);
                mediaPlayer.start();
                mediaPlayer.setOnCompletionListener(BeepHelper.this);
            }
        };
        handler.post(runbeep);
    }

    public void alarm(){
        if(!preference.isMusicOn()){
            return;
        }

        Runnable runbeep = new Runnable() {
            @Override
            public void run() {
                MediaPlayer mediaPlayer = MediaPlayer.create(context, R.raw.alarm);
                mediaPlayer.setLooping(false);
                mediaPlayer.start();
                mediaPlayer.setOnCompletionListener(BeepHelper.this);
            }
        };
        handler.post(runbeep);
    }

    public void alarmLong(){
        if(!preference.isMusicOn()){
            return;
        }

        Runnable runbeep = new Runnable() {
            @Override
            public void run() {
                MediaPlayer mediaPlayer = MediaPlayer.create(context, R.raw.alarm_long);
                mediaPlayer.setLooping(false);
                mediaPlayer.start();
                mediaPlayer.setOnCompletionListener(BeepHelper.this);
            }
        };
        handler.post(runbeep);
    }

    public void show(){
        if(!preference.isMusicOn()){
            return;
        }
        try{
            if(mediaPlayer != null){
                if(mediaPlayer.isPlaying()){
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
            }
        }catch (Exception e){

        }
        Runnable runbeep = new Runnable() {
            @Override
            public void run() {
                mediaPlayer = MediaPlayer.create(context, R.raw.show);
                mediaPlayer.setLooping(false);
                mediaPlayer.start();
                mediaPlayer.setOnCompletionListener(BeepHelper.this);
            }
        };
        handler.post(runbeep);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        mp.stop();
        mp.release();
    }
}
