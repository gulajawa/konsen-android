package net.arizalsaputro.konsen.helper;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.annotation.Nullable;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.util.Constant;

import static net.arizalsaputro.konsen.util.Constant.MAX_VOLUME;

/**
 * Created by muharizals on 03/04/2017.
 */

public class MyMediaHelper extends Service implements MediaPlayer.OnPreparedListener,MediaPlayer.OnErrorListener{

    MediaPlayer mediaPlayer = null;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mediaPlayer = MediaPlayer.create(this, R.raw.background_music);
        mediaPlayer.setLooping(true);
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnErrorListener(this);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        try{
            if(mediaPlayer.isPlaying()) {
                for (int i = 0; i<=MAX_VOLUME; i++){
                    final float volume = (float) (1 - (Math.log(MAX_VOLUME - i) / Math.log(MAX_VOLUME)));
                    mediaPlayer.setVolume(volume, volume);
                }
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
            }
        }catch (Exception e){

        }
        super.onDestroy();
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        if(!mp.isPlaying()){
            mp.start();
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int i, int i1) {
        try{
            mp.stop();
            mp.release();

            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }catch (Exception e){

        }
        return false;
    }
}
