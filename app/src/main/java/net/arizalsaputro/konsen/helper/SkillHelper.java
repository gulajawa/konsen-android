package net.arizalsaputro.konsen.helper;

import android.content.Context;

import net.arizalsaputro.konsen.util.Constant;
import net.arizalsaputro.konsen.util.MySharedPreference;
import net.arizalsaputro.konsen.util.MyUtil;

/**
 * Created by muharizals on 04/04/2017.
 */

public class SkillHelper {
    private Context context;
    private MySharedPreference preference;

    public SkillHelper(Context context) {
        this.context = context;
        preference = new MySharedPreference(this.context);
    }

    public void calculate(int suppostlengt,int questionlength,int correct,int wrong){
        if(correct == 0 && wrong == 0){
           return;
        }
        setAccuracy(questionlength,correct,wrong);
        setSpeed(suppostlengt,questionlength,correct,wrong);
        setCalculation(suppostlengt,questionlength,correct,wrong);

    }

    private void setAccuracy(int questionlength,int correct,int wrong){
        try{

            float c_a = (float) MyUtil.getAccuracy(correct,wrong);
            preference.setFloatProperty(Constant.ACCURACY,c_a);
        }catch (Exception e){

        }
    }

    private void setSpeed(int suppostlengt,int questionlength,int correct,int wrong){
        try{
            float hsl = MyUtil.getSpeed(correct,suppostlengt);
            preference.setFloatProperty(Constant.SPEED,hsl);
        }catch (Exception e){

        }

    }


    private void setCalculation(int suppostlengt,int questionlength,int correct,int wrong){
        try{
            float c_a = MyUtil.getCalculation(questionlength,correct);
            preference.setFloatProperty(Constant.CALCULATION,c_a);
        }catch (Exception e){

        }
    }




    public float getAccuracy(){
        return preference.getFloatProperty(Constant.ACCURACY);
    }
    public float getSpeed(){
        return preference.getFloatProperty(Constant.SPEED);
    }

    public float getCalculation(){
        return preference.getFloatProperty(Constant.CALCULATION);
    }

    public float getJudgment(){
        return preference.getFloatProperty(Constant.JUDGMENT);
    }
}
