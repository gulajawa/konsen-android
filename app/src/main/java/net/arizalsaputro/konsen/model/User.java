package net.arizalsaputro.konsen.model;

import android.media.Image;

import com.beltaief.reactivefb.ReactiveFB;
import com.beltaief.reactivefb.actions.ReactiveLogin;
import com.beltaief.reactivefb.requests.ReactiveRequest;
import com.beltaief.reactivefb.util.Utils;
import com.google.gson.annotations.SerializedName;

/**
 * Created by muharizals on 07/04/2017.
 */

public class User {
    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("picture")
    private UserPicture picture;

    @SerializedName("email")
    private String email;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public UserPicture getPicture() {
        return picture;
    }

    public String getEmail() {
        return email;
    }

    public class UserPicture{
        @SerializedName("data")
        private PictureData data;

        public PictureData getData() {
            return data;
        }
    }

    public class PictureData{
        @SerializedName("url")
        private String url;

        public String getUrl() {
            return url;
        }
    }
}
