package net.arizalsaputro.konsen.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by muharizals on 09/04/2017.
 */

public class NameValuePair {
    @SerializedName("nameValuePairs")
    private Person data;

    public Person getData() {
        return data;
    }
}
