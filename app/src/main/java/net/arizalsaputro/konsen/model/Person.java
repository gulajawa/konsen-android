package net.arizalsaputro.konsen.model;

import android.databinding.BaseObservable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by muharizals on 06/04/2017.
 */

public class Person {

    @SerializedName("_id")
    public String id;

    @SerializedName("fbid")
    public String fbid;

    @SerializedName("ranking")
    public int ranking;

    @SerializedName("name")
    public String name;

    @SerializedName("picture")
    public String image;

    @SerializedName("email")
    public String email;

    @SerializedName("point")
    public double point;


    public Person(String name, String image, int ranking, double point) {
        this.name = name;
        this.image = image;
        this.ranking = ranking;
        this.point = point;
    }

    public Person(String id, String fbid, int ranking, String name, String image, String email, double point) {
        this.id = id;
        this.fbid = fbid;
        this.ranking = ranking;
        this.name = name;
        this.image = image;
        this.email = email;
        this.point = point;
    }

    public String getMyRanking(){
        return String.valueOf(this.ranking);
    }

    public String getMyPoint(){
        return String.valueOf(point);
    }
}
