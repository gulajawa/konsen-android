package net.arizalsaputro.konsen.model;

import android.app.Activity;
import android.graphics.drawable.Drawable;

/**
 * Created by muharizals on 03/04/2017.
 */

public class Game {
    public String name;
    public Drawable image;
    public Class aClass;

    public Game(String name, Drawable image, Class aClass) {
        this.name = name;
        this.image = image;
        this.aClass = aClass;
    }

    public String getName() {
        return name;
    }

    public Drawable getImage() {
        return image;
    }


}
