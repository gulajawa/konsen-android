package net.arizalsaputro.konsen.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.view.View;

import net.arizalsaputro.konsen.BR;

/**
 * Created by muharizals on 06/04/2017.
 */

public class LeaderGame extends BaseObservable{
    private int order,background,invisible;
    private boolean visibility;


    public LeaderGame(int order, int background, boolean visibility) {
        this.order = order;
        this.background = background;
        this.visibility = visibility;
        invisible = View.INVISIBLE;
    }

    @Bindable
    public int getOrder() {
        return order;
    }

    @Bindable
    public int getInvisible() {
        return invisible;
    }


    @Bindable
    public int getBackground() {
        return background;
    }

    @Bindable
    public boolean getVisibility() {
        return visibility;
    }

    public void setInvisible(int invisible) {
        this.invisible = invisible;
        notifyPropertyChanged(BR.invisible);
    }

    public void setOrder(int order) {
        this.order = order;
        notifyPropertyChanged(BR.order);
    }

    public void setBackground(int background) {
        this.background = background;
        notifyPropertyChanged(BR.background);
    }

    public void setVisibility(boolean visibility) {
        this.visibility = visibility;
        notifyPropertyChanged(BR.visibility);
    }
}
