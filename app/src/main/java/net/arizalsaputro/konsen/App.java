package net.arizalsaputro.konsen;

import android.app.Application;
import android.content.Context;

import com.beltaief.reactivefb.ReactiveFB;
import com.beltaief.reactivefb.SimpleFacebookConfiguration;
import com.beltaief.reactivefb.util.PermissionHelper;
import com.facebook.login.DefaultAudience;

import net.arizalsaputro.konsen.rest.KonsenFactory;
import net.arizalsaputro.konsen.rest.KonsenService;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by muharizals on 07/04/2017.
 */

public class App extends Application {

    private KonsenService konsenService;
    private Scheduler scheduler;

    private static App get(Context context){
        return (App)context.getApplicationContext();
    }

    public static App create(Context context){
        return App.get(context);
    }

    public KonsenService getKonsenService(){
        if(konsenService == null) konsenService = KonsenFactory.create();
        return konsenService;
    }


    public Scheduler subscribeScheduler(){
        if(scheduler == null) scheduler = Schedulers.io();
        return scheduler;
    }

    public void setKonsenService(KonsenService konsenService) {
        this.konsenService = konsenService;
    }

    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        PermissionHelper[] permissions = new PermissionHelper[]{
                PermissionHelper.USER_ABOUT_ME,
                PermissionHelper.EMAIL};
        SimpleFacebookConfiguration simpleFacebookConfiguration = new SimpleFacebookConfiguration.Builder()
                .setAppId(String.valueOf(R.string.facebook_app_id))
                .setPermissions(permissions)
                .setDefaultAudience(DefaultAudience.EVERYONE)
                .setAskForAllPermissionsAtOnce(false)
                .build();
        ReactiveFB.sdkInitialize(this);
        ReactiveFB.setConfiguration(simpleFacebookConfiguration);
    }
}
