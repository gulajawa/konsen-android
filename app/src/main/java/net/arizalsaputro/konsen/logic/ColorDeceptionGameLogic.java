package net.arizalsaputro.konsen.logic;

import android.content.Context;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.util.MyUtil;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by muharizals on 05/04/2017.
 */

public class ColorDeceptionGameLogic {
    public int questioncount,correct,incorrect;
    private GameLogicInterface mListener;
    private Context context;

    private ArrayList<String> colorlist;
    private ArrayList<Integer> intcolorlist;

    private String tmpCorrect_answer_String_color;
    private int tmpCorrect_answer_int_color;

    private String tmpWrong_answer_String_color;
    private int tmpWrong_anser_int_color;


    private int correct_position;

    public ColorDeceptionGameLogic(GameLogicInterface listener, Context context){
        reset();
        this.mListener = listener;
        this.context = context;
        colorlist = new ArrayList<>();
        intcolorlist  = new ArrayList<>();
        initColor();
    }

    private void initColor(){
        colorlist.add(context.getString(R.string.color_blue));
        colorlist.add(context.getString(R.string.color_red));
        colorlist.add(context.getString(R.string.color_green));
        colorlist.add(context.getString(R.string.color_yellow));
        colorlist.add(context.getString(R.string.color_black));
        colorlist.add(context.getString(R.string.color_white));


        intcolorlist.add(R.color.color_blue);
        intcolorlist.add(R.color.color_red);
        intcolorlist.add(R.color.color_green);
        intcolorlist.add(R.color.color_yellow);
        intcolorlist.add(R.color.color_black);
        intcolorlist.add(R.color.color_white);

    }


    public void reset(){
        questioncount = correct = incorrect =0;
    }

    public void doLogic() {
        final int randomtru = getRandom(0,colorlist.size());
        tmpWrong_answer_String_color = colorlist.get(randomtru);
        tmpWrong_anser_int_color = intcolorlist.get(randomtru);

        new Thread (
                new Runnable() {
                    public void run() {

                        int fixedrandom1 = makeFixedRandom();
                        int no_fixedrandom2 = makeFixedRandom2(fixedrandom1,randomtru);
                        tmpCorrect_answer_String_color = colorlist.get(fixedrandom1);
                        tmpCorrect_answer_int_color = intcolorlist.get(no_fixedrandom2);
                        correct_position = getRandom(1,3);
                        mListener.update();
                    }

                    private int makeFixedRandom(){
                        int tmp_t_c = getRandom(0,colorlist.size());
                        if(tmp_t_c == randomtru){
                            return makeFixedRandom();
                        }
                        return tmp_t_c;
                    }

                    private int makeFixedRandom2(int fix,int fix2){
                        int tmp_t_c = getRandom(0,colorlist.size());
                        if(tmp_t_c == fix || tmp_t_c == fix2){
                            return makeFixedRandom2(fix,fix2);
                        }
                        return tmp_t_c;
                    }


                }
        ).start();

    }

    public boolean checkAnswer(int position){
        if(position == correct_position){
            return true;
        }
        return false;
    }

    public int getTextColor(int backgroundAnswer){
        if(backgroundAnswer == R.color.color_white || backgroundAnswer == R.color.color_yellow){
            return R.color.color_black;
        }
        return R.color.color_white;
    }


    public int getIntColorAnswer(int position){
        if(position == correct_position){
            return this.tmpCorrect_answer_int_color;
        }
        return this.tmpWrong_anser_int_color;
    }

    public String getStringColorAnswer(int position){
        if(position == correct_position){
            return this.tmpCorrect_answer_String_color;
        }
        return this.tmpWrong_answer_String_color;
    }


    private int getRandom(int min,int max){
        Random random = new Random();
        int result = 5;
        try {
            result= random.nextInt(max - min ) + min;
        }catch (IllegalArgumentException e){

        }catch (Exception e){

        }
        return result;
    }

    public float getAccuracy(){
        if(questioncount <= 0){
            return 0.1F;
        }

        return MyUtil.getAccuracy(correct,incorrect);
    }

}
