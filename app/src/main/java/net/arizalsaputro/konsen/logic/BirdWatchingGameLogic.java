package net.arizalsaputro.konsen.logic;

import android.content.Context;
import android.databinding.ObservableArrayList;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.util.MyUtil;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by muharizals on 05/04/2017.
 */

public class BirdWatchingGameLogic {
    public int questioncount,correct,incorrect;
    private GameLogicInterface mListener;
    private Context context;

    public int correct_color;

    private ObservableArrayList<Integer> list;

    private ArrayList<Integer> intcolorlist;

    public BirdWatchingGameLogic(GameLogicInterface mListener, Context context) {
        this.mListener = mListener;
        this.context = context;
        list = new ObservableArrayList<>();
        reset();
        intcolorlist = new ArrayList<>();
        initColor();
    }

    public boolean checkAnswer(int color){
        if(color == correct_color){
            return true;
        }
        return false;
    }

    private void initColor(){
        intcolorlist.add(R.color.color_blue);
        intcolorlist.add(R.color.midnight_blue);
        intcolorlist.add(R.color.color_green);
        intcolorlist.add(R.color.color_yellow);
    }

    public ObservableArrayList<Integer> getList(){
        return list;
    }

    public void doLogic(){
        list.clear();
        new Thread (
                new Runnable() {
                    public void run() {
                        int color_1 = getRandom(0,intcolorlist.size());
                        int color_2 = makeFixedRandom(0,intcolorlist.size(),color_1);

                        int[] listcolor = new int[2];

                        listcolor[0] = intcolorlist.get(color_1);
                        listcolor[1] = intcolorlist.get(color_2);

                        int tru_position = getRandom(0,2);//iki sing posisine limo


                        correct_color = listcolor[tru_position];

                        int counter1=0,counter2=0;

                        while ((counter1 < check(1,tru_position)) || (counter2 < check(2,tru_position))){
                            int randomlagi = getRandom(0,2);
                            if(randomlagi == 0){

                                if(tru_position == 0){
                                    if(counter1 < 5){
                                        list.add(listcolor[0]);
                                        counter1++;
                                    }
                                }else{
                                    if(counter1 < 4){
                                        list.add(listcolor[0]);
                                        counter1++;
                                    }
                                }
                            }
                            else if(randomlagi == 1){
                                if(tru_position == 1){
                                    if(counter2 < 5){
                                        list.add(listcolor[1]);
                                        counter2++;
                                    }
                                }else{
                                    if(counter2 < 4){
                                        list.add(listcolor[1]);
                                        counter2++;
                                    }
                                }
                            }



                        }


                        mListener.update();
                    }


                    private int check(int p,int t){
                        if(p == 1){
                            if(t == 0){
                                return 5;
                            }else {
                                return 4;
                            }
                        }
                        if(p == 2){
                            if(t == 1){
                                return 5;
                            }else{
                                return 4;
                            }
                        }
                        return 1;
                    }

                    private int makeFixedRandom(int min,int max,int comparator){
                        int tmpr = getRandom(min,max);
                        if(tmpr == comparator){
                            return makeFixedRandom(min,max,comparator);
                        }
                        return tmpr;
                    }



                }
        ).start();
    }

    private int getRandom(int min,int max){
        Random random = new Random();
        int result = 5;
        try {
            result= random.nextInt(max - min ) + min;
        }catch (IllegalArgumentException e){

        }catch (Exception e){

        }
        return result;
    }


    public float getAccuracy(){

        return MyUtil.getAccuracy(correct,incorrect);
    }

    public void reset(){
        questioncount = correct = incorrect = correct_color=0;
    }
}
