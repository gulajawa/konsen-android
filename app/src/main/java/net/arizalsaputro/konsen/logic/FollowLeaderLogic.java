package net.arizalsaputro.konsen.logic;

import android.content.Context;
import android.databinding.ObservableArrayList;

import net.arizalsaputro.konsen.R;
import net.arizalsaputro.konsen.model.LeaderGame;
import net.arizalsaputro.konsen.util.MyUtil;

import java.util.Random;

/**
 * Created by muharizals on 06/04/2017.
 */

public class FollowLeaderLogic {
    public int questioncount,correct,incorrect;
    private GameLogicInterface mListener;

    private int levelcount;
    private int ascendingordecending;


    private ObservableArrayList<LeaderGame> list;
    public int answer_length ;

    public FollowLeaderLogic(GameLogicInterface mListener) {
        this.mListener = mListener;
        reset();
        init();
    }

    private void init(){
        list = new ObservableArrayList<>();
        for (int i=0;i<9;i++){
            list.add(new LeaderGame(0,R.color.midnight_blue,false));
        }

    }

    private void setNolandFalseList(){
        for (int i=0;i<9;i++){
            LeaderGame tmp = list.get(i);
            tmp.setOrder(0);
            tmp.setVisibility(false);
            list.set(i,tmp);
        }
    }

    private int getLengthFromLvlCount(int lvc){
        if(lvc < 4){
            return 3;
        }
        return 4;

    }

    public ObservableArrayList<LeaderGame> getList(){
        return list;
    }

    public void doLogic(){
        setNolandFalseList();
        answer_length = getLengthFromLvlCount(levelcount);
            int lenghcount = 0;
            while (lenghcount < answer_length){
                int randomposition = getRandom(0,list.size());
                if(list.get(randomposition).getOrder() == 0){
                    LeaderGame tmp = list.get(randomposition);
                    tmp.setOrder(lenghcount+1);
                    tmp.setVisibility(true);
                    list.set(randomposition,tmp);
                    lenghcount++;
                }
            }

        mListener.update();
        levelcount++;
    }

    private int getRandom(int min, int max){
        Random random = new Random();
        int result = 5;
        try {
            result= random.nextInt(max - min ) + min;
        }catch (IllegalArgumentException e){

        }catch (Exception e){

        }
        return result;
    }



    public float getAccuracy(){

        return MyUtil.getAccuracy(correct,incorrect);
    }

    public void reset(){
        questioncount = correct = incorrect = levelcount =0;
    }
}
