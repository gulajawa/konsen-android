package net.arizalsaputro.konsen.logic;

import net.arizalsaputro.konsen.util.MyUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by muharizals on 04/04/2017.
 */

public class OperationGameLogic {
    private int arithmetic;
    public int num_1=0,num_2=0,num_3=0;

    private int lvlcount = 1;

    private GameLogicInterface mListener;

    public OperationGameLogic(GameLogicInterface mListener){
        this.mListener = mListener;
    }

    private int getLength(){

        if (lvlcount == 1){
            return 4;
        }
        if(lvlcount == 2){
            return 5;
        }
        if(lvlcount == 3){
            return 6;
        }
        if(lvlcount == 4){
            return 7;
        }
        if(lvlcount >= 5){
            return 10;
        }
        return 20;
    }

    public boolean checkAnswer(int position){
        switch (position){
            case 1:
                if(num_1+num_2 == num_3){
                    return true;
                }
                break;
            case 2:
                if(num_1-num_2 == num_3){
                    return true;
                }
                break;
            case 3:
                if(num_1*num_2 == num_3){
                    return true;
                }
                break;
            case 4:
                if(num_1/num_2 == num_3){
                    return true;
                }
                break;
        }
        return false;
    }

    public void doLogic(){
        arithmetic = getRandom(1,5);

        int length = getLength();


        num_1 = getRandom(1,length);
        num_2 = getRandom(1,length);


        switch (arithmetic){
            case 1:
                num_3 = num_1 + num_2;
                mListener.update();
                break;
            case 2:
                num_3 = num_1 - num_2;
                mListener.update();
                break;
            case 3:
                num_3 = num_1*num_2;
                mListener.update();
                break;
            case 4:
                new Thread (
                        new Runnable() {
                            public void run() {

                                List<Integer> list = makeDevide(num_1,num_2);
                                num_1 = list.get(0);
                                num_2 = list.get(1);
                                num_3 = num_1/num_2;
                                mListener.update();
                            }

                            private List<Integer> makeDevide(int n1,int n2){
                                List<Integer> list = new ArrayList<>();

                                if(checkDevide(n1,n2)){
                                    list.add(n1);
                                    list.add(n2);
                                    return list;
                                }else{
                                    n1 = getRandom(1,10);
                                    n2 = getRandom(num_1,10);
                                    return makeDevide(n1,n2);
                                }

                            }

                        }
                ).start();
                break;
        }
        lvlcount = lvlcount +1;
    }


    public void resrtLvlcount(){
        this.lvlcount = 1;
    }


    private boolean checkDevide(int i,int j){
        if(i%j == 0){
            return true;
        }
        return false;
    }


    private int getRandom(int min,int max){
        Random random = new Random();
        int result = 5;
        try {
            result= random.nextInt(max - min ) + min;
        }catch (IllegalArgumentException e){

        }catch (Exception e){

        }
        return result;
    }


    public float getAccuracy(int count,int correct,int incorrect){
        if(count <= 0){
            return 0.1F;
        }

        return MyUtil.getAccuracy(correct,incorrect);
    }

}
