package net.arizalsaputro.konsen.logic;

/**
 * Created by muharizals on 04/04/2017.
 */

public interface GameLogicInterface {
    void update();
}
